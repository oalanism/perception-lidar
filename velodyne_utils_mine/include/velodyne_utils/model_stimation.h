#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/features/normal_3d.h>
#include <pcl/features/feature.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/features/fpfh.h>
#include <pcl/features/impl/fpfh.hpp>
#include <pcl/recognition/implicit_shape_model.h>
#include <pcl/recognition/impl/implicit_shape_model.hpp>
#include <string>
#include "velodyne_utils/point_types.h"


namespace velodyne_utils{

  using namespace std;
  using namespace pcl;
  using PointT = PointXYZIRC;
  using PointCloudT = pcl::PointCloud<PointT>;


  class ModelStimation{
    private:


    public:
      int getClass(int nbCar, int nbPeople);
      int detectObject(PointCloud<PointXYZ>::Ptr& testing_cloud );

  };

  int ModelStimation::getClass(int nbCar, int nbPeople){
    if(nbCar > 5 || nbPeople > 5){
      if(nbCar > nbPeople)
        return 1;
      else
        return 2;
    }
    else
      return 3;

  }

  int ModelStimation::detectObject(PointCloud<PointXYZ>::Ptr& testing_cloud ){

    string fileModel = "trained_ism_model.txt";
    ism::ImplicitShapeModelEstimation<153, PointXYZ, Normal>::ISMModelPtr model (new features::ISMModel);
    model->loadModelFromfile(fileModel);


    int testing_class_car = static_cast<int> (strtol ("0", 0, 10));
    int testing_class_people = static_cast<int> (strtol ("1", 0, 10));

    int nbCar;
    int nbPeople;

    FPFHEstimation<PointXYZ, Normal, Histogram<153> >::Ptr fpfh (new FPFHEstimation<PointXYZ, Normal, Histogram<153> >);
    fpfh->setRadiusSearch (1.0);
    Feature< PointXYZ, Histogram<153> >::Ptr feature_estimator(fpfh);

    ism::ImplicitShapeModelEstimation<153, PointXYZ, Normal> ism;
    ism.setFeatureEstimator(feature_estimator);
    ism.setSamplingSize (2.0f);

    PointCloud<Normal>::Ptr testing_normals = (new PointCloud<Normal>)->makeShared ();

    NormalEstimation<PointXYZ, Normal> normal_estimator;
    normal_estimator.setRadiusSearch (1.0);
    normal_estimator.setInputCloud (testing_cloud);
    normal_estimator.compute (*testing_normals);


    features::ISMVoteList<PointXYZ>::Ptr vote_list_car = ism.findObjects (
      model,
      testing_cloud,
      testing_normals,
      testing_class_car);

    features::ISMVoteList<PointXYZ>::Ptr vote_list_people = ism.findObjects (
      model,
      testing_cloud,
      testing_normals,
      testing_class_people);

    nbCar = vote_list_car->getNumberOfVotes();
    nbPeople = vote_list_people->getNumberOfVotes();

    return getClass(nbCar, nbPeople);


  }

}
