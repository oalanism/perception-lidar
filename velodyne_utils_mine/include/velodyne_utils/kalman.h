#include <Eigen/Dense>
#include <math.h>

namespace velodyne_utils{

  class Kalman{
    private:
      Eigen::VectorXf xk{Eigen::VectorXf(4)};
      Eigen::MatrixXf Pk{Eigen::MatrixXf(4, 4)};
      Eigen::VectorXf fk{Eigen::VectorXf(4)};
      Eigen::MatrixXf Fk{Eigen::MatrixXf(4, 4)};
      Eigen::MatrixXf Qk{Eigen::MatrixXf(4, 4)};
      Eigen::MatrixXf Hk{Eigen::MatrixXf(3, 4)};
      Eigen::MatrixXf Rk{Eigen::MatrixXf(3, 3)};

    public:
      void kalman(float x, float y, float theta, float v);
      void update(float x, float y, float theta);
      float velocity(){return xk[3];};
  };

  void Kalman::kalman(float x, float y, float theta, float v){
    float a = pow(0.2, 2);
    float b = a;
    float c = pow(0.1, 2);
    float d = 100;
    Pk << a, 0, 0, 0,
          0, b, 0, 0,
          0, 0, c, 0,
          0, 0, 0, d;

    xk << x, y, theta, v;

    fk << x + v*0.1*cos(theta), y+ v*0.1*sin(theta), theta, 0;

    Fk = Eigen::MatrixXf::Identity(4, 4);
    Qk = Eigen::MatrixXf::Identity(4, 4);
    Qk(0, 0) = pow(0.01, 2); Qk(1, 1) = pow(0.01, 2); Qk(2, 2) = 10; Qk(3, 3) = 100;
    Hk = Eigen::MatrixXf::Zero(3, 4);
    Hk(0, 0) = 1; Hk(1, 1) = 1; Hk(2, 2) = 1;
    Rk = Eigen::MatrixXf::Identity(3, 3);
    Rk(0, 0) = pow(0.2, 2); Rk(1, 1) = pow(0.2, 2); Rk(2, 2) = 0.1;
  }

  void Kalman::update(float x, float y, float theta){
    Eigen::Vector3f zk(3);
    zk << x, y, theta;
    Eigen::Vector3f yk(3);
    yk = zk - Hk*xk;
    Eigen::MatrixXf Sk(3, 3);
    Sk = Hk*Pk*Hk.transpose() + Rk;
    Eigen::MatrixXf Kk(3, 3);
    Kk = Pk*Hk.transpose()*Sk.inverse();
    Eigen::VectorXf xk1(4);
    xk = xk + Kk*yk;
    Pk = (Eigen::MatrixXf::Identity(4, 4) - Kk*Hk)*Pk;

  }
}
