#pragma once

#define PCL_NO_PRECOMPILE
#include <pcl/PointIndices.h>

#include "point_types.h"

namespace velodyne_utils
{

bool clusterEuclidean  (pcl::PointCloud<PointXYZIRC>::ConstPtr pc, std::vector<pcl::PointCloud<PointXYZIRC>::Ptr>& clusters, float distanceMax, size_t pointsMin, size_t pointsMax);
bool clusterScanLineRun(pcl::PointCloud<PointXYZIRC>::ConstPtr pc, std::vector<pcl::PointCloud<PointXYZIRC>::Ptr>& clusters, float distanceMax, size_t pointsMin, size_t pointsMax, int lookBack);

namespace tools
{

bool clustersToClustered(const std::vector<pcl::PointCloud<PointXYZIRC>::Ptr>& clusters, pcl::PointCloud<PointXYZIRC>::Ptr clustered);
bool clusteredToClusters(pcl::PointCloud<PointXYZIRC>::ConstPtr clustered, std::vector<pcl::PointCloud<PointXYZIRC>::Ptr>& clusters);
bool clusteredToIndices(pcl::PointCloud<PointXYZIRC>::ConstPtr clustered, std::vector<pcl::PointIndices::Ptr>& clustersIndices);

} // namespace tools

} // namespace velodyne_utils
