#pragma once

#include "point_types.h"

namespace velodyne_utils {

inline bool inRange(const PointXYZIRC& p, float thresh) {
    return p.x*p.x + p.y*p.y < thresh*thresh;
}
inline bool inRange(const PointXYZIRC& pA, const PointXYZIRC& pB, float thresh) {
    return inRange(PointXYZIRC{pB.x-pA.x, pB.y-pA.y}, thresh);
}

inline bool inRange3(const PointXYZIRC& p, float thresh) {
    return p.x*p.x + p.y*p.y + p.z*p.z < thresh*thresh;
}
inline bool inRange3(const PointXYZIRC& pA, const PointXYZIRC& pB, float thresh) {
    return inRange3(PointXYZIRC{pB.x-pA.x, pB.y-pA.y, pB.z-pA.z}, thresh);
}

bool filterHeight            (pcl::PointCloud<PointXYZIRC>::ConstPtr pc, pcl::PointCloud<PointXYZIRC>::Ptr inliers, pcl::PointCloud<PointXYZIRC>::Ptr outliers, float zmin, float zmax);
bool filterIntensity         (pcl::PointCloud<PointXYZIRC>::ConstPtr pc, pcl::PointCloud<PointXYZIRC>::Ptr inliers, pcl::PointCloud<PointXYZIRC>::Ptr outliers, float imin, float imax);
bool filterStatisticalOutlier(pcl::PointCloud<PointXYZIRC>::ConstPtr pc, pcl::PointCloud<PointXYZIRC>::Ptr inliers, pcl::PointCloud<PointXYZIRC>::Ptr outliers, float mean, float stdDev);

}
