#include <Eigen/Dense>
#include "velodyne_utils/point_types.h"
#include "velodyne_utils/l_shappe.h"

namespace velodyne_utils{

  using namespace std;
  using namespace pcl;
  using PointT = PointXYZIRC;
  using PointCloudT = pcl::PointCloud<PointT>;

  class Cluster{
    private:
      Eigen::Vector3f mean_values;
      int id;
      PointCloudT::Ptr points;
      int size;
      LShappe lshappe;
      double velocity;
      int label;
    public:
      double meanX() { return mean_values(0); };
      double meanY() { return mean_values(1); };
      double meanZ() { return mean_values(2); };
      PointCloudT::Ptr Points() {return points;};
      int Id(){return id;};
      void setId(int idCluster) ;
      void setPoints(PointCloudT::Ptr pointsCluster);
      void setSize(int sizeCluster);
      void cluster(int idCluster, PointCloudT::Ptr pointsCluster, int labelCluster);
      void update(PointCloudT::Ptr pointsCluster);
      void calculMeans(PointCloudT::Ptr pointsCluster);
      double Velocity() {return velocity;};
      BoundingBox Bbox(){return lshappe.Bbox();};
      int Label() {return label;}

  };

  void Cluster::setPoints(PointCloudT::Ptr pointsCluster){
    points = pointsCluster;
  }

  void Cluster::setId(int idCluster){
    id = idCluster;
  }

  void Cluster::setSize(int sizeCluster){
    size = sizeCluster;
  }

  void Cluster::cluster(int idCluster, PointCloudT::Ptr pointsCluster, int labelCluster){
    setId(idCluster);
    setPoints(pointsCluster);
    setSize(pointsCluster->size());
    calculMeans(pointsCluster);
    lshappe.lShappeComputing(pointsCluster);
    double orientation = lshappe.orientation();
    lshappe.initKalman(meanX(), meanY(), orientation, 0);
    velocity = 0;
    label = labelCluster;
  }

  void Cluster::update(PointCloudT::Ptr pointsCluster){
    setPoints(pointsCluster);
    setSize(pointsCluster->size());
    calculMeans(pointsCluster);
    lshappe.lShappeComputing(pointsCluster);
    double orientation = lshappe.orientation();
    lshappe.updateKalman(meanX(), meanY(), orientation);
    velocity = lshappe.Velocity();
  }

  void Cluster::calculMeans(PointCloudT::Ptr pointsCluster){
    double mean_x = 0, mean_y = 0, mean_z = 0;

    mean_x = (pointsCluster->getMatrixXfMap().row(0).sum())/(pointsCluster->getMatrixXfMap().rows());
    mean_y = (pointsCluster->getMatrixXfMap().row(1).sum())/(pointsCluster->getMatrixXfMap().rows());
    mean_z = (pointsCluster->getMatrixXfMap().row(2).sum())/(pointsCluster->getMatrixXfMap().rows());

    mean_values(0) = mean_x;
    mean_values(1) = mean_y;
    mean_values(2) = mean_z;
  }


}
