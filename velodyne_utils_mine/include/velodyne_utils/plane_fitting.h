#pragma once

#include "point_types.h"

namespace velodyne_utils {
    bool planeFitRansac(pcl::PointCloud<PointXYZIRC>::ConstPtr pc, pcl::PointCloud<PointXYZIRC>::Ptr inliers, pcl::PointCloud<PointXYZIRC>::Ptr outliers, float maxDistToPlane);
    bool planeFitPca   (pcl::PointCloud<PointXYZIRC>::ConstPtr pc, pcl::PointCloud<PointXYZIRC>::Ptr inliers, pcl::PointCloud<PointXYZIRC>::Ptr outliers, float maxDistToPlane, float heightMin, float heightMax, float radiusMin, float radiusMax, size_t nSeeds, size_t nIter);
}
