#pragma once

#include "point_types.h"

#include <tf/transform_listener.h>

#include <pcl/filters/extract_indices.h>
#include <pcl_ros/point_cloud.h>
#include <pcl_ros/transforms.h>
#include <pcl_conversions/pcl_conversions.h>

#include <geometry_msgs/TwistWithCovarianceStamped.h>

namespace velodyne_utils {

class RollingBuffer
{
public:
    RollingBuffer();
    RollingBuffer(const std::string& frame_id, double length);

    inline void setFrameId(std::string frame_id) { _frame_id = frame_id; }
    inline void setLength(double length) { _length = length; }


    void addTwist(const geometry_msgs::TwistWithCovarianceStamped::ConstPtr& twistMsg);

    void addPointCloud(pcl::PointCloud<PointXYZIRC>::ConstPtr pc);

    tf::Transform getTransform(uint64_t start, uint64_t end);

    inline pcl::PointCloud<PointXYZIRC>::Ptr getPointCloud() { return _megaPointCloud; }


private:
    std::string _frame_id;
    double _length;

    pcl::PointCloud<PointXYZIRC>::Ptr _megaPointCloud;
    int _megaSize;

    // Transform
    tf::TransformListener _tfListener;

    std::deque<double> _pcTimes;
    std::deque<geometry_msgs::TwistWithCovarianceStamped::ConstPtr> _twistBuffer;
};

} // namespace velodyne_utils
