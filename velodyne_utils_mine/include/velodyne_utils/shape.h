#pragma once

#include <geometry_msgs/Polygon.h>
#include <jsk_recognition_msgs/BoundingBox.h>

#include "velodyne_utils/PolygonWithDescriptors.h"

#include "velodyne_utils/point_types.h"

namespace velodyne_utils
{

bool lexicographicalSort(const Eigen::Matrix<float, 2, Eigen::Dynamic>& in, Eigen::Matrix<float, 2, Eigen::Dynamic>& out);
bool computeHull(const Eigen::Matrix<float, 2, Eigen::Dynamic>& P, Eigen::Matrix<float, 2, Eigen::Dynamic>& H);

bool computeDelimitation(pcl::PointCloud<PointXYZIRC>::ConstPtr pc, Eigen::Matrix<float, 2, Eigen::Dynamic>& delim, float heightMax, float distMin, float distMax, float zMax, float radialStep);
bool computeBoundingBox(pcl::PointCloud<PointXYZIRC>::ConstPtr clustered, jsk_recognition_msgs::BoundingBox& bbox);
bool computeHull(pcl::PointCloud<PointXYZIRC>::ConstPtr pc, geometry_msgs::Polygon& hull, float z = 0);

bool computeDescriptors(pcl::PointCloud<PointXYZIRC>::ConstPtr pc, velodyne_utils::PolygonWithDescriptors& polygon);

namespace tools
{

bool pointcloudToPolygon(const pcl::PointCloud<PointXYZIRC>& pc, geometry_msgs::Polygon& hull, float z = 0);

} // namespace tools

} // namespace velodyne_utils
