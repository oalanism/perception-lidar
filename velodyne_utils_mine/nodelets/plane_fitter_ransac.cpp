#include <ros/ros.h>
#include <nodelet/nodelet.h>
#include <dynamic_reconfigure/server.h>
#include <velodyne_utils/PlaneFitterRansacConfig.h>

#include "velodyne_utils/plane_fitting.h"

namespace velodyne_utils
{
using PointT = PointXYZIRC;
using PointCloudT = pcl::PointCloud<PointT>;

class PlaneFitterRansacNodelet : public nodelet::Nodelet {
    // ROS Related
    ros::Subscriber sub_points_;
    ros::Publisher  pub_inliers_, pub_outliers_;

    // Parameters
    std::shared_ptr<dynamic_reconfigure::Server<velodyne_utils::PlaneFitterRansacConfig>> config_server_;
    float dist_max_;

    void onInit() override {
        sub_points_  = getNodeHandle().subscribe("input", 2, &PlaneFitterRansacNodelet::callbackVelodyne, this);
        pub_inliers_ = getNodeHandle().advertise<PointCloudT>("inliers", 2);
        pub_outliers_ = getNodeHandle().advertise<PointCloudT>("outliers", 2);

        config_server_ = std::make_shared<dynamic_reconfigure::Server<velodyne_utils::PlaneFitterRansacConfig>>(getPrivateNodeHandle());
        config_server_->setCallback(boost::bind(&PlaneFitterRansacNodelet::callbackConfig, this, _1, _2));
    }

    void callbackConfig(const PlaneFitterRansacConfig& config, uint32_t) {
        dist_max_ = config.dist_max;
    }

    void callbackVelodyne(PointCloudT::ConstPtr pc) {
        PointCloudT::Ptr inliers { (pub_inliers_.getNumSubscribers()!=0) ? new PointCloudT : nullptr };
        PointCloudT::Ptr outliers { (pub_outliers_.getNumSubscribers()!=0) ? new PointCloudT : nullptr };
        planeFitRansac(pc, inliers, outliers, dist_max_);
        if(inliers)  { inliers->header  = pc->header; pub_inliers_.publish(inliers); }
        if(outliers) { outliers->header = pc->header; pub_outliers_.publish(outliers); }
    }
};

} // namespace velodyne_utils

// Register this plugin with pluginlib.  Names must match nodelets.xml.
// parameters: class type, base class type
#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(velodyne_utils::PlaneFitterRansacNodelet, nodelet::Nodelet)
