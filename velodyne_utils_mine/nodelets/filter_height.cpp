#include <ros/ros.h>
#include <nodelet/nodelet.h>
#include <dynamic_reconfigure/server.h>
#include <velodyne_utils/FilterHeightConfig.h>

#include "velodyne_utils/filtering.h"

namespace velodyne_utils {

using PointT = PointXYZIRC;
using PointCloudT = pcl::PointCloud<PointT>;

class FilterHeightNodelet : public nodelet::Nodelet {
    // ROS Related
    ros::Subscriber sub_points_;
    ros::Publisher  pub_inliers_, pub_outliers_;

    // Parameters
    std::shared_ptr<dynamic_reconfigure::Server<velodyne_utils::FilterHeightConfig>> config_server_;
    float height_min_, height_max_;

    void onInit() override {
        sub_points_  = getNodeHandle().subscribe("input", 2, &FilterHeightNodelet::callbackVelodyne, this);
        pub_inliers_ = getNodeHandle().advertise<PointCloudT>("inliers",  2);
        pub_outliers_ = getNodeHandle().advertise<PointCloudT>("outliers", 2);

        config_server_ = std::make_shared<dynamic_reconfigure::Server<velodyne_utils::FilterHeightConfig>>(getPrivateNodeHandle());
        config_server_->setCallback(boost::bind(&FilterHeightNodelet::callbackConfig, this, _1, _2));
    }

    void callbackConfig(const FilterHeightConfig& config, uint32_t) {
        height_min_ = config.height_min;
        height_max_ = config.height_max;
    }

    void callbackVelodyne(PointCloudT::ConstPtr pc) {
        PointCloudT::Ptr inliers { (pub_inliers_.getNumSubscribers()!=0) ? new PointCloudT : nullptr };
        PointCloudT::Ptr outliers { (pub_outliers_.getNumSubscribers()!=0) ? new PointCloudT : nullptr };
        filterHeight(pc, inliers, outliers, height_min_, height_max_);
        if(inliers)  { inliers->header  = pc->header; pub_inliers_.publish(inliers); }
        if(outliers) { outliers->header = pc->header; pub_outliers_.publish(outliers); }
    }
};

} // namespace velodyne_utils

// Register this plugin with pluginlib.  Names must match nodelets.xml.
// parameters: class type, base class type
#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(velodyne_utils::FilterHeightNodelet, nodelet::Nodelet)
