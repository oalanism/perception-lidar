#include <ros/ros.h>
#include <nodelet/nodelet.h>
#include <dynamic_reconfigure/server.h>
#include <velodyne_utils/ClusterScanLineRunConfig.h>
//#include <jsk_recognition_msgs/Points.h>

#include <time.h>

#include "velodyne_utils/clustering.h"

namespace velodyne_utils {

using PointT = PointXYZIRC;
using PointCloudT = pcl::PointCloud<PointT>;

class ClusterScanLineRunNodelet : public nodelet::Nodelet {
    // ROS Related
    ros::Subscriber sub_points_;
    ros::Publisher  pub_clustered_, pub_clusters_;

    // Parameters
    std::shared_ptr<dynamic_reconfigure::Server<velodyne_utils::ClusterScanLineRunConfig>> config_server_;
    float dist_max_;
    size_t points_min_, points_max_;
    int look_back_;

    void onInit() override {
        sub_points_  = getMTNodeHandle().subscribe("input", 2, &ClusterScanLineRunNodelet::callbackVelodyne, this);
        pub_clustered_ = getMTNodeHandle().advertise<PointCloudT>("clustered",  2);
        //pub_clusters_ = getMTNodeHandle().advertise<jsk_recognition_msgs::Points>("clusters", 2);

        dist_max_ = 0.0f;
        points_min_ = points_max_ = 0;
        config_server_ = std::make_shared<dynamic_reconfigure::Server<velodyne_utils::ClusterScanLineRunConfig>>(getPrivateNodeHandle());
        config_server_->setCallback(boost::bind(&ClusterScanLineRunNodelet::callbackConfig, this, _1, _2));
    }

    void callbackConfig(const ClusterScanLineRunConfig& config, uint32_t) {
        dist_max_ = config.dist_max;
        points_min_ = config.points_min;
        points_max_ = config.points_max;
        look_back_  = config.look_back;
    }

    void callbackVelodyne(PointCloudT::ConstPtr pc) {
        std::vector<PointCloudT::Ptr> clusters;
        PointCloudT::Ptr clustered { (pub_clustered_.getNumSubscribers()!=0) ? new PointCloudT : nullptr };
        clusterScanLineRun(pc, clusters, dist_max_, points_min_, points_max_, look_back_);
        //if(pub_clusters_.getNumSubscribers()!=0)  { inliers->header  = pc->header; pub_inliers_.publish(inliers); }
        if(clustered) {
            clustered->header = pc->header;
            clustered->reserve(pc->size());
            tools::clustersToClustered(clusters, clustered);
            pub_clustered_.publish(clustered);
        }
    }
};

} // namespace velodyne_utils

// Register this plugin with pluginlib.  Names must match nodelets.xml
#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(velodyne_utils::ClusterScanLineRunNodelet, nodelet::Nodelet)
