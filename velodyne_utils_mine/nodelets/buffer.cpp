#include <ros/ros.h>
#include <nodelet/nodelet.h>
#include <dynamic_reconfigure/server.h>
#include <velodyne_utils/BufferConfig.h>

#include "velodyne_utils/buffering.h"

namespace velodyne_utils {

using PointT = PointXYZIRC;
using PointCloudT = pcl::PointCloud<PointT>;

class BufferNodelet : public nodelet::Nodelet {
    // ROS Related
    ros::Subscriber sub_points;
    ros::Subscriber sub_twist;
    ros::Publisher  pub_points;

    std::shared_ptr<dynamic_reconfigure::Server<velodyne_utils::BufferConfig>> _config_server;
    double _length = 1;

    RollingBuffer _buffer;

    void onInit() override {
        sub_points = getNodeHandle().subscribe("input", 2, &BufferNodelet::callbackPointCloud, this);
        sub_twist  = getNodeHandle().subscribe("twist", 2, &BufferNodelet::callbackTwist, this);
        pub_points = getNodeHandle().advertise<PointCloudT>("buffer",  2);

        _buffer.setFrameId("base");
        _buffer.setLength(_length);

        _config_server = std::make_shared<dynamic_reconfigure::Server<velodyne_utils::BufferConfig>>(getPrivateNodeHandle());
        _config_server->setCallback(boost::bind(&BufferNodelet::callbackConfig, this, _1, _2));

    }

    void callbackConfig(const BufferConfig& config, uint32_t) {
        _buffer.setLength(config.length);
    }

    void callbackPointCloud(PointCloudT::ConstPtr pc) {
        _buffer.addPointCloud(pc);
        pub_points.publish(_buffer.getPointCloud());
    }

    void callbackTwist(const geometry_msgs::TwistWithCovarianceStamped::ConstPtr twistMsg) {
        _buffer.addTwist(twistMsg);
    }
};

} // namespace velodyne_utils

// Register this plugin with pluginlib.  Names must match nodelets.xml.
// parameters: class type, base class type
#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(velodyne_utils::BufferNodelet, nodelet::Nodelet)
