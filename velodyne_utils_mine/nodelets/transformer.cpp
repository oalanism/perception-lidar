#include <ros/ros.h>
#include <nodelet/nodelet.h>
#include <tf2_ros/transform_listener.h>
#include <tf2_eigen/tf2_eigen.h>

#define PCL_NO_PRECOMPILE
#include <pcl/common/transforms.h>
#include <pcl_conversions/pcl_conversions.h>

#include <dynamic_reconfigure/server.h>
#include <velodyne_utils/TransformerConfig.h>

#include "velodyne_utils/point_types.h"

namespace velodyne_utils {

using PointCloudT = pcl::PointCloud<PointXYZIRC>;

class TransformerNodelet : public nodelet::Nodelet {
    // ROS Related
    ros::Subscriber sub_points_;
    ros::Publisher  pub_points_;
    std::shared_ptr<tf2_ros::Buffer> tf_buffer_;
    std::shared_ptr<tf2_ros::TransformListener> tf_listener_;

    // Parameters
    std::shared_ptr<dynamic_reconfigure::Server<velodyne_utils::TransformerConfig>> config_server_;
    std::string frame_from_, frame_to_, framename_to_;

    void onInit() override {
        sub_points_  = getNodeHandle().subscribe("input", 2, &TransformerNodelet::callbackVelodyne, this);
        pub_points_ = getNodeHandle().advertise<PointCloudT>("output", 2);

        config_server_ = std::make_shared<dynamic_reconfigure::Server<velodyne_utils::TransformerConfig>>(getPrivateNodeHandle());
        config_server_->setCallback(boost::bind(&TransformerNodelet::callbackConfig, this, _1, _2));

        tf_buffer_ = std::make_shared<tf2_ros::Buffer>(ros::Duration(1));
        tf_listener_ = std::make_shared<tf2_ros::TransformListener>(*tf_buffer_);
    }

    void callbackConfig(const TransformerConfig& config, uint32_t) {
        frame_from_ = config.frame_from;
        frame_to_ = config.frame_to;
        framename_to_ = config.framename_to.empty() ? config.frame_to : config.framename_to;
    }

    void callbackVelodyne(PointCloudT::ConstPtr cloudFrom) {
        Eigen::Affine3d transform;

        if(getTf(cloudFrom, transform)) {
            PointCloudT::Ptr cloudTo { new PointCloudT };
            pcl::transformPointCloud(*cloudFrom, *cloudTo, transform);
            cloudTo->header = cloudFrom->header;
            cloudTo->header.frame_id = framename_to_; // Optionnaly rename frame
            pub_points_.publish(cloudTo);
        }
    }

    bool getTf(PointCloudT::ConstPtr cloud, Eigen::Affine3d& transform, const ros::Duration& timeout = ros::Duration(0.01)) const {
        try {
            const ros::Time timestamp = pcl_conversions::fromPCL(cloud->header).stamp;
            const geometry_msgs::TransformStamped tf = tf_buffer_->lookupTransform(frame_to_, frame_from_, timestamp, timeout);
            transform = tf2::transformToEigen(tf);
        }
        catch (tf2::ConnectivityException &ex) {
            ROS_WARN_STREAM(ex.what());
            return false;
        }
        catch (tf2::TransformException &ex) {
            ROS_WARN_STREAM(ex.what());
            return false;
        }

        return true;
    }
};

} // namespace velodyne_utils

// Register this plugin with pluginlib. Names must match nodelets.xml.
#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(velodyne_utils::TransformerNodelet, nodelet::Nodelet)
