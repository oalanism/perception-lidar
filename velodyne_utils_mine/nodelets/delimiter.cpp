#include <ros/ros.h>
#include <nodelet/nodelet.h>
#include <pcl_conversions/pcl_conversions.h>
#include <geometry_msgs/PolygonStamped.h>

#include <dynamic_reconfigure/server.h>
#include <velodyne_utils/DelimiterConfig.h>

#include "velodyne_utils/shape.h"

namespace velodyne_utils {

using PointT = PointXYZIRC;
using PointCloudT = pcl::PointCloud<PointT>;
using Polygon2 = Eigen::Matrix<float, 2, Eigen::Dynamic>;

class DelimiterNodelet : public nodelet::Nodelet {
    // ROS Related
    ros::Subscriber sub_points_;
    ros::Publisher  pub_polygon_;

    // Parameters
    std::shared_ptr<dynamic_reconfigure::Server<velodyne_utils::DelimiterConfig>> config_server_;
    float radial_step_, dist_min_, dist_max_, height_max_, z_max_;

    void onInit() override {
        sub_points_  = getNodeHandle().subscribe("input", 2, &DelimiterNodelet::callbackVelodyne, this);
        pub_polygon_ = getNodeHandle().advertise<geometry_msgs::PolygonStamped>("area", 2);

        config_server_ = std::make_shared<dynamic_reconfigure::Server<velodyne_utils::DelimiterConfig>>(getPrivateNodeHandle());
        config_server_->setCallback(boost::bind(&DelimiterNodelet::callbackConfig, this, _1, _2));
    }

    void callbackConfig(const DelimiterConfig& config, uint32_t) {
        radial_step_ = config.radial_step;
        dist_min_ = config.dist_min;
        dist_max_ = config.dist_max;
        height_max_ = config.height_max;
        z_max_ = config.z_max;
    }

    void callbackVelodyne(PointCloudT::ConstPtr pc) {
        // Compute the polygon vertices
        Polygon2 delimitation;
        computeDelimitation(pc, delimitation, height_max_, dist_min_, dist_max_, z_max_, radial_step_);

        // Create a polygon message from the vertices
        geometry_msgs::PolygonStamped::Ptr polygonMsg { new geometry_msgs::PolygonStamped };
        polygonMsg->polygon.points.resize(delimitation.cols()+1);

        for(Eigen::Index i=0; i<delimitation.cols(); i++) {
            polygonMsg->polygon.points[i].x = delimitation(0,i);
            polygonMsg->polygon.points[i].y = delimitation(1,i);
            polygonMsg->polygon.points[i].z = height_max_;
        }

        polygonMsg->polygon.points[delimitation.cols()] = polygonMsg->polygon.points[0];
        polygonMsg->header = pcl_conversions::fromPCL(pc->header);
        pub_polygon_.publish(polygonMsg);
    }
};

} // namespace velodyne_utils

// Register this plugin with pluginlib. Names must match nodelets.xml.
#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(velodyne_utils::DelimiterNodelet, nodelet::Nodelet)
