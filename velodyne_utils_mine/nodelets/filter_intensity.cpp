#include <ros/ros.h>
#include <nodelet/nodelet.h>
#include <dynamic_reconfigure/server.h>
#include <velodyne_utils/FilterIntensityConfig.h>

#include "velodyne_utils/filtering.h"

namespace velodyne_utils {

using PointT = PointXYZIRC;
using PointCloudT = pcl::PointCloud<PointT>;

class FilterIntensityNodelet : public nodelet::Nodelet {
    // ROS Related
    ros::Subscriber sub_points_;
    ros::Publisher  pub_inliers_, pub_outliers_;

    // Parameters
    std::shared_ptr<dynamic_reconfigure::Server<velodyne_utils::FilterIntensityConfig>> config_server_;
    float intensity_min_, intensity_max_;

    void onInit() override {
        sub_points_  = getNodeHandle().subscribe("input", 2, &FilterIntensityNodelet::callbackVelodyne, this);
        pub_inliers_ = getNodeHandle().advertise<PointCloudT>("inliers",  2);
        pub_outliers_ = getNodeHandle().advertise<PointCloudT>("outliers", 2);

        config_server_ = std::make_shared<dynamic_reconfigure::Server<velodyne_utils::FilterIntensityConfig>>(getPrivateNodeHandle());
        config_server_->setCallback(boost::bind(&FilterIntensityNodelet::callbackConfig, this, _1, _2));
    }

    void callbackConfig(const FilterIntensityConfig& config, uint32_t) {
        intensity_min_ = config.intensity_min;
        intensity_max_ = config.intensity_max;
    }

    void callbackVelodyne(PointCloudT::ConstPtr pc) {
        PointCloudT::Ptr inliers { (pub_inliers_.getNumSubscribers()!=0) ? new PointCloudT : nullptr };
        PointCloudT::Ptr outliers { (pub_outliers_.getNumSubscribers()!=0) ? new PointCloudT : nullptr };
        filterIntensity(pc, inliers, outliers, intensity_min_, intensity_max_);
        if(inliers)  { inliers->header  = pc->header; pub_inliers_.publish(inliers); }
        if(outliers) { outliers->header = pc->header; pub_outliers_.publish(outliers); }
    }
};

} // namespace velodyne_utils

// Register this plugin with pluginlib.  Names must match nodelets.xml.
// parameters: class type, base class type
#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(velodyne_utils::FilterIntensityNodelet, nodelet::Nodelet)
