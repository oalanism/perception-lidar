#include <ros/ros.h>
#include <nodelet/nodelet.h>
#include <dynamic_reconfigure/server.h>
#include <velodyne_utils/FilterStatisticalConfig.h>

#include "velodyne_utils/filtering.h"

namespace velodyne_utils {

using PointT = PointXYZIRC;
using PointCloudT = pcl::PointCloud<PointT>;

class FilterStatisticalNodelet : public nodelet::Nodelet {
    // ROS Related
    ros::Subscriber sub_points_;
    ros::Publisher  pub_inliers_, pub_outliers_;

    // Parameters
    std::shared_ptr<dynamic_reconfigure::Server<velodyne_utils::FilterStatisticalConfig>> config_server_;
    float mean_, stddev_;

    void onInit() override {
        sub_points_  = getNodeHandle().subscribe("input", 2, &FilterStatisticalNodelet::callbackVelodyne, this);
        pub_inliers_ = getNodeHandle().advertise<PointCloudT>("inliers",  2);
        pub_outliers_ = getNodeHandle().advertise<PointCloudT>("outliers", 2);

        config_server_ = std::make_shared<dynamic_reconfigure::Server<velodyne_utils::FilterStatisticalConfig>>(getPrivateNodeHandle());
        config_server_->setCallback(boost::bind(&FilterStatisticalNodelet::callbackConfig, this, _1, _2));
    }

    void callbackConfig(const FilterStatisticalConfig& config, uint32_t) {
        mean_ = config.mean;
        stddev_ = config.stddev;
    }

    void callbackVelodyne(PointCloudT::ConstPtr pc) {
        PointCloudT::Ptr inliers { (pub_inliers_.getNumSubscribers()!=0) ? new PointCloudT : nullptr };
        PointCloudT::Ptr outliers { (pub_outliers_.getNumSubscribers()!=0) ? new PointCloudT : nullptr };
        filterStatisticalOutlier(pc, inliers, outliers, mean_, stddev_);
        if(inliers)  { inliers->header  = pc->header; pub_inliers_.publish(inliers); }
        if(outliers) { outliers->header = pc->header; pub_outliers_.publish(outliers); }
    }
};

} // namespace velodyne_utils

// Register this plugin with pluginlib.  Names must match nodelets.xml.
// parameters: class type, base class type
#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(velodyne_utils::FilterStatisticalNodelet, nodelet::Nodelet)
