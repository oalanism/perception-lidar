#include <ros/ros.h>
#include <nodelet/nodelet.h>
#include <pcl_conversions/pcl_conversions.h>
#include <jsk_recognition_msgs/BoundingBoxArray.h>

#include <dynamic_reconfigure/server.h>
#include <velodyne_utils/ShaperBoundingBoxConfig.h>

#include "velodyne_utils/clustering.h"
#include "velodyne_utils/shape.h"
#include "velodyne_utils/model_stimation.h"
#include "velodyne_utils/cluster.h"
#include "velodyne_utils/BoxesArray.h"
#include "velodyne_utils/BoxMessage.h"

namespace velodyne_utils {

using PointT = PointXYZIRC;
using PointCloudT = pcl::PointCloud<PointT>;
using Polygon2 = Eigen::Matrix<float, 2, Eigen::Dynamic>;

class ShaperBoundingBoxNodelet : public nodelet::Nodelet {
    // ROS Related
    ros::Subscriber sub_points_;
    ros::Publisher  pub_bboxes_;
    ros::Publisher  pub_boxes_;
    jsk_recognition_msgs::BoundingBoxArray::Ptr bboxes { new jsk_recognition_msgs::BoundingBoxArray };
    ros::Time begin;
    std::vector<Cluster> Clusters;
    bool first;
    velodyne_utils::BoxesArray boxes;

    ModelStimation ms;

    // Parameters
    std::shared_ptr<dynamic_reconfigure::Server<velodyne_utils::ShaperBoundingBoxConfig>> config_server_;

    void onInit() override {
        first = true;
        begin = ros::Time::now();

        sub_points_  = getNodeHandle().subscribe("input", 10, &ShaperBoundingBoxNodelet::callbackVelodyne, this);
        pub_bboxes_ = getNodeHandle().advertise<jsk_recognition_msgs::BoundingBoxArray>("bboxes", 10);
        pub_boxes_ = getNodeHandle().advertise<velodyne_utils::BoxesArray>("Boxes", 10);

        config_server_ = std::make_shared<dynamic_reconfigure::Server<velodyne_utils::ShaperBoundingBoxConfig>>(getPrivateNodeHandle());
        config_server_->setCallback(boost::bind(&ShaperBoundingBoxNodelet::callbackConfig, this, _1, _2));
    }

    void callbackConfig(const ShaperBoundingBoxConfig& config, uint32_t) {
        ;
    }

    void callbackVelodyne(PointCloudT::ConstPtr pc) {
        ros::Time now = ros::Time::now();
        ros::Duration d(0.5);

        if(now < begin){ begin = now; Clusters.clear(); first = true;}

        std::vector<pair<int, int> > pairs;
        std::vector<bool> Ccmatches(Clusters.size(), false);


        if((now - begin) > d){
          begin = now;
          std::vector<PointCloudT::Ptr> clusters;
          tools::clusteredToClusters(pc, clusters);
          std::vector<bool> Pcmatches(clusters.size(), false);
          velodyne_utils::BoxMessage b;
          b.centerX = 0;
          b.centerY = 0;
          b.centerZ = 0;
          bboxes->header = pcl_conversions::fromPCL(pc->header);
          bboxes->boxes.resize(clusters.size());
          //int cn;
          //printf("Size of clusters : %ld\n", clusters.size());
          double euclidean[clusters.size()][Clusters.size()];

          for(size_t i = 0; i < clusters.size(); i++) {

            double mean_x = 0, mean_y = 0, mean_z = 0;
            float euclidean_distance = 0.5;
            unsigned int position;

            mean_x = (clusters[i]->getMatrixXfMap().row(0).sum())/(clusters[i]->getMatrixXfMap().rows());
            mean_y = (clusters[i]->getMatrixXfMap().row(1).sum())/(clusters[i]->getMatrixXfMap().rows());
            mean_z = (clusters[i]->getMatrixXfMap().row(2).sum())/(clusters[i]->getMatrixXfMap().rows());
            float min_distance = euclidean_distance;
            for (size_t j = 0; j < Clusters.size(); j++) {
              euclidean[i][j] = abs(mean_x - Clusters[j].meanX()) + abs(mean_y - Clusters[j].meanY()) + abs(mean_z - Clusters[j].meanZ());
              if(min_distance > euclidean[i][j]){
                position = j;
                min_distance = euclidean[i][j];
              }
            }

            if(min_distance < euclidean_distance){
              Clusters[position].update(clusters[i]);
              Pcmatches[i] = true; Ccmatches[position] = true;
            }





              //bboxes->boxes[i].value = cn;

              //printf("Box Cluster %ld\n", i);
          }

          int idLast;

          if(Clusters.size() > 0){
            idLast = Clusters[Clusters.size()-1].Id();
          }
          else
            idLast = -1;

          for (size_t i = 0; i < Ccmatches.size(); i++) {
            if(!Ccmatches[i]) Clusters.erase(Clusters.begin() +i );
          }

          for (size_t i = 0; i < Pcmatches.size(); i++) {
            if(!Pcmatches[i]){

              PointCloud<PointXYZ>::Ptr testing_cloud (new PointCloud<PointXYZ> );
              testing_cloud->reserve(256);

              testing_cloud->width = clusters[i]->width;
              testing_cloud->height = clusters[i]->height;
              testing_cloud->resize(testing_cloud->width * testing_cloud->height);

              testing_cloud->getMatrixXfMap().row(0) = clusters[i]->getMatrixXfMap().row(0);
              testing_cloud->getMatrixXfMap().row(1) = clusters[i]->getMatrixXfMap().row(1);
              testing_cloud->getMatrixXfMap().row(2) = clusters[i]->getMatrixXfMap().row(2);

              int cn = ms.detectObject(testing_cloud);
              Cluster c;
              c.cluster(idLast+1, clusters[i], cn);
              Clusters.push_back(c);
              idLast++;
            }
          }

          for (size_t i = 0; i < Clusters.size(); i++) {
            //computeBoundingBox(Clusters[i].Points(), bboxes->boxes[i]);



            //std::cout << "Deteccion" << '\n';



            bboxes->boxes[i] = Clusters[i].Bbox();
            bboxes->boxes[i].label = Clusters[i].Id();
            bboxes->boxes[i].header = bboxes->header;

            velodyne_utils::BoxMessage b;
            b.centerX = bboxes->boxes[i].pose.position.x;
            b.centerY = bboxes->boxes[i].pose.position.y;
            b.centerZ = bboxes->boxes[i].pose.position.z;
            b.orientationX = bboxes->boxes[i].pose.orientation.x;
            b.orientationY = bboxes->boxes[i].pose.orientation.y;
            b.orientationZ = bboxes->boxes[i].pose.orientation.z;
            b.orientationW = bboxes->boxes[i].pose.orientation.w;
            b.dimensionX = bboxes->boxes[i].dimensions.x;
            b.dimensionY = bboxes->boxes[i].dimensions.y;
            b.dimensionZ = bboxes->boxes[i].dimensions.z;
            b.label = Clusters[i].Label();
            b.velocity = Clusters[i].Velocity();
            boxes.Boxes.push_back(b);
          }
          pub_boxes_.publish(boxes);
          pub_bboxes_.publish(bboxes);
          //std::cin >> cn;
        }

        first = false;


    }
};

} // namespace velodyne_utils

// Register this plugin with pluginlib. Names must match nodelets.xml.
#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(velodyne_utils::ShaperBoundingBoxNodelet, nodelet::Nodelet)
