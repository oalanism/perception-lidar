#define PCL_NO_PRECOMPILE

#include <ros/ros.h>
#include <nodelet/nodelet.h>
#include <pcl/surface/convex_hull.h>
#include <pcl_conversions/pcl_conversions.h>
#include <jsk_recognition_msgs/PolygonArray.h>

#include <dynamic_reconfigure/server.h>
#include <velodyne_utils/ShaperHullConfig.h>

#include <velodyne_utils/PolygonWithDescriptorsArray.h>

#include "velodyne_utils/clustering.h"
#include "velodyne_utils/shape.h"

namespace velodyne_utils {

using PointT = PointXYZIRC;
using PointCloudT = pcl::PointCloud<PointT>;
using Polygon2 = Eigen::Matrix<float, 2, Eigen::Dynamic>;

class ShaperHullNodelet : public nodelet::Nodelet {
    // ROS Related
    ros::Subscriber sub_points_;
    ros::Publisher  pub_hulls_;
    ros::Publisher  pub_polygons_;

    // Parameters
    std::shared_ptr<dynamic_reconfigure::Server<velodyne_utils::ShaperHullConfig>> config_server_;

    void onInit() override {
        sub_points_  = getNodeHandle().subscribe("input", 10, &ShaperHullNodelet::callbackVelodyne, this);
        pub_hulls_ = getNodeHandle().advertise<jsk_recognition_msgs::PolygonArray>("hulls", 10);
        pub_polygons_ = getNodeHandle().advertise<velodyne_utils::PolygonWithDescriptorsArray>("polygons", 10);

        config_server_ = std::make_shared<dynamic_reconfigure::Server<velodyne_utils::ShaperHullConfig>>(getPrivateNodeHandle());
        config_server_->setCallback(boost::bind(&ShaperHullNodelet::callbackConfig, this, _1, _2));
    }

    void callbackConfig(const ShaperHullConfig& config, uint32_t) {
        ;
    }

    void callbackVelodyne(PointCloudT::ConstPtr pc) {
        std::vector<PointCloudT::Ptr> clusters;
        tools::clusteredToClusters(pc, clusters);

        std::vector<PointCloudT> hulls;
        hulls.resize(clusters.size());

        // JSK message
        jsk_recognition_msgs::PolygonArray::Ptr hullPolygons { new jsk_recognition_msgs::PolygonArray };
        hullPolygons->header = pcl_conversions::fromPCL(pc->header);
        hullPolygons->labels.resize(clusters.size());
        hullPolygons->polygons.resize(clusters.size());

        // Custom message
        PolygonWithDescriptorsArray::Ptr polygonsMsg { new PolygonWithDescriptorsArray };
        polygonsMsg->header = pcl_conversions::fromPCL(pc->header);
        polygonsMsg->polygons.resize(clusters.size());

        uint16_t clusterId = 0;
        for(size_t i = 0; i<clusters.size(); i++) {
            if(clusters[i]->size()>3) {
                pcl::ConvexHull<PointT> hullConstructor;
                hullConstructor.setInputCloud(clusters[i]);
                hullConstructor.setDimension(2);
                hullConstructor.reconstruct(hulls[i]);

                tools::pointcloudToPolygon(hulls[i], hullPolygons->polygons[clusterId].polygon);
                hullPolygons->labels[clusterId] = clusters[i]->points[0].clusterId;
                hullPolygons->polygons[clusterId].header = hullPolygons->header;

                polygonsMsg->polygons[clusterId].polygon = hullPolygons->polygons[clusterId].polygon;
                polygonsMsg->polygons[clusterId].zmin = clusters[i]->getMatrixXfMap().row(2).minCoeff();
                polygonsMsg->polygons[clusterId].zmax = clusters[i]->getMatrixXfMap().row(2).maxCoeff();
                computeDescriptors(clusters[i], polygonsMsg->polygons.at(clusterId));

                clusterId++;
            }
        }

        hullPolygons->labels.resize(clusterId);
        hullPolygons->polygons.resize(clusterId);
        pub_hulls_.publish(hullPolygons);

        pub_polygons_.publish(polygonsMsg);
    }
};

} // namespace velodyne_utils

// Register this plugin with pluginlib. Names must match nodelets.xml.
#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(velodyne_utils::ShaperHullNodelet, nodelet::Nodelet)
