#include <ros/ros.h>
#include <nodelet/nodelet.h>
#include <dynamic_reconfigure/server.h>
#include <velodyne_utils/PlaneFitterPcaConfig.h>

#include "velodyne_utils/plane_fitting.h"

namespace velodyne_utils
{
using PointT = PointXYZIRC;
using PointCloudT = pcl::PointCloud<PointT>;

class PlaneFitterPcaNodelet : public nodelet::Nodelet {
    // ROS Related
    ros::Subscriber sub_points_;
    ros::Publisher  pub_inliers_, pub_outliers_;

    // Parameters
    std::shared_ptr<dynamic_reconfigure::Server<velodyne_utils::PlaneFitterPcaConfig>> config_server_;
    float dist_max_, height_min_, height_max_, radius_min_, radius_max_;
    size_t n_seeds_, n_iter_;

    void onInit() override {
        sub_points_  = getMTNodeHandle().subscribe("input", 2, &PlaneFitterPcaNodelet::callbackVelodyne, this);
        pub_inliers_ = getMTNodeHandle().advertise<PointCloudT>("inliers", 2);
        pub_outliers_ = getMTNodeHandle().advertise<PointCloudT>("outliers", 2);

        config_server_ = std::make_shared<dynamic_reconfigure::Server<velodyne_utils::PlaneFitterPcaConfig>>(getPrivateNodeHandle());
        config_server_->setCallback(boost::bind(&PlaneFitterPcaNodelet::callbackConfig, this, _1, _2));
    }

    void callbackConfig(const PlaneFitterPcaConfig& config, uint32_t) {
        dist_max_ = config.dist_max;
        height_min_ = config.height_min;
        height_max_ = config.height_max;
        radius_min_ = config.radius_min;
        radius_max_ = config.radius_max;
        n_seeds_ = config.n_seeds;
        n_iter_ = config.n_iter;
    }

    void callbackVelodyne(PointCloudT::ConstPtr pc) {
        PointCloudT::Ptr inliers { new PointCloudT }; // Create one anyway, because it is used to compute the plane
        PointCloudT::Ptr outliers { (pub_outliers_.getNumSubscribers()!=0) ? new PointCloudT : nullptr };
        planeFitPca(pc, inliers, outliers, dist_max_, height_min_, height_max_, radius_min_, radius_max_, n_seeds_, n_iter_);
        if(inliers)  { inliers->header  = pc->header; pub_inliers_.publish(inliers); }
        if(outliers) { outliers->header = pc->header; pub_outliers_.publish(outliers); }
    }
};

} // namespace velodyne_utils

// Register this plugin with pluginlib.  Names must match nodelets.xml.
// parameters: class type, base class type
#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(velodyne_utils::PlaneFitterPcaNodelet, nodelet::Nodelet)
