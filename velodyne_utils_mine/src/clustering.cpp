#define PCL_NO_PRECOMPILE
#include <unordered_map>
#include <pcl/segmentation/extract_clusters.h>

/* GPU is not available in the default version of PCL, it needs to be compiled manually.
#include <pcl/gpu/octree/octree.hpp>
#include <pcl/gpu/containers/device_array.hpp>
#include <pcl/gpu/segmentation/gpu_extract_clusters.h>
#include <pcl/gpu/segmentation/impl/gpu_extract_clusters.hpp>
*/

#include "velodyne_utils/filtering.h"
#include "velodyne_utils/clustering.h"

namespace velodyne_utils {

using PointT = PointXYZIRC;
using PointCloudT = pcl::PointCloud<PointT>;

bool clusterEuclidean(PointCloudT::ConstPtr pc, std::vector<PointCloudT::Ptr>& clusters, float distanceMax, size_t pointsMin, size_t pointsMax) {
    if(!pc || pc->points.empty()) return false;

    pcl::search::KdTree<PointT>::Ptr tree(new pcl::search::KdTree<PointT>);
    tree->setInputCloud(pc);

    std::vector<pcl::PointIndices> clusterIndices;

    pcl::EuclideanClusterExtraction<PointT> ec;
    ec.setClusterTolerance(distanceMax);
    ec.setMinClusterSize(pointsMin);
    ec.setMaxClusterSize(pointsMax);
    ec.setSearchMethod(tree);
    ec.setInputCloud(pc);
    ec.extract(clusterIndices);

    clusters.resize(clusterIndices.size());
    for(size_t i=0; i<clusterIndices.size(); i++) {
        clusters[i].reset(new PointCloudT(*pc, clusterIndices[i].indices));
        for(PointT& p : *clusters[i]) {
            p.clusterId = i;
        }
    }

    return true;
}

/*
bool clusterEuclideanGpu(PointCloudT::ConstPtr pc, std::vector<PointCloudT::Ptr>& clusters, float distanceMax, size_t pointsMin, size_t pointsMax) {
    if(!pc || pc->points.empty()) return false;

    pcl::gpu::Octree::PointCloud cloud_device;
    cloud_device.upload(pc->points);

    pcl::gpu::Octree::Ptr octree_device (new pcl::gpu::Octree);
    octree_device->setCloud(cloud_device);
    octree_device->build();
    std::vector<pcl::PointIndices> cluster_indices_gpu;
    pcl::gpu::EuclideanClusterExtraction gec;
    gec.setClusterTolerance (distanceMax); // 2cm
    gec.setMinClusterSize (pointsMin);
    gec.setMaxClusterSize (pointsMax);
    gec.setSearchMethod (octree_device);
    gec.setHostCloud( pc);
    gec.extract (cluster_indices_gpu);

    clusters.resize(cluster_indices_gpu.size());
    for(size_t i=0; i<cluster_indices_gpu.size(); i++) {
        clusters[i].reset(new PointCloudT(*pc, cluster_indices_gpu[i].indices));
        for(PointT& p : *clusters[i]) {
            p.clusterId = i;
        }
    }

    return true;
}
*/


const size_t nRings = 32;
const size_t nRows = 2251;
const uint16_t nonClusterId = 0;
const uint16_t groundClusterId = 1;

void mergeClusters(std::vector<std::vector<PointT*>>& clusters, uint16_t sourceClusterId, uint16_t targetClusterId) {
    // Reserve enough memory to hold the new points
    clusters[targetClusterId].reserve(clusters[targetClusterId].size() + clusters[sourceClusterId].size());

    // Copy each point of the source cluster to the target cluster, changing the point cluster id
    for (PointT* p : clusters[sourceClusterId]) {
        p->clusterId = targetClusterId;
        clusters[targetClusterId].push_back(p);
    }

    clusters[sourceClusterId].clear();
}

void findClustersInRing(const std::vector<size_t>& ringIndices, const std::array<PointT*, nRows>& ringPoints, std::vector<std::vector<PointT*>>& clusters, float distanceMax, int lookBack) {
    // Create a new cluster from the first point
    if (ringIndices.size()>0) {
        PointT* p0 = ringPoints[ringIndices[0]];
        p0->clusterId = clusters.size();
        clusters.emplace_back(std::vector<PointT*>{p0});
    }

    // Along the ring
    for (size_t i=1; i<ringIndices.size(); i++) {
        PointT* pi;
        PointT* pi1 = ringPoints[ringIndices[i]];

        // Propagate the cluster to the next point if close enough
        bool clustered = false;
        for (int j = 1; j <= i && j <= lookBack; ++j) {
            pi = ringPoints[ringIndices[i-j]];
            if (inRange(*pi, *pi1, distanceMax)) {
                pi1->clusterId = pi->clusterId;
                clusters[pi->clusterId].push_back(pi1);
                clustered = true;
                break;
            }
        }
        if (!clustered) {
            // Otherwise make it a new cluster
            pi1->clusterId = clusters.size();
            clusters.emplace_back(std::vector<PointT*>{pi1});
        }
    }

    // Because rings are circular, the start and end of a ring can be in the same cluster
    if (ringIndices.size() > 1) {
        PointT* p0 = ringPoints[ringIndices[0]];
        PointT* pl = ringPoints[ringIndices[ringIndices.size()-1]];

        // In case they are in different cluster but close enough, merge the two clusters
        if (p0->clusterId!=pl->clusterId && inRange(*p0, *pl, distanceMax)) {
            mergeClusters(clusters, pl->clusterId, p0->clusterId);
        }
    }
}

void propagateClustersBetweenRings(const std::vector<size_t>& ringIndices, const std::array<std::array<PointT*, nRows>, nRings>& rings, std::vector<std::vector<PointT*>>& clusters, size_t ring, float distanceMax) {
    // For each point of the ring
    for (size_t row : ringIndices) {
        PointT* pi = rings[ring][row];

        // For each ring below the current one
        for (size_t lowerRing = ring; lowerRing>0; lowerRing--) {
            PointT* pnn = rings[lowerRing-1][row];

            // Merge the two clusters if the point below is near enough and not already merged
            if (pnn && pi->clusterId!=pnn->clusterId && inRange(*pi, *pnn, distanceMax)) {
                mergeClusters(clusters, pnn->clusterId, pi->clusterId);
            }
        }
    }
}

bool clusterScanLineRun(PointCloudT::ConstPtr pc, std::vector<PointCloudT::Ptr>& clustersOut, float distanceMax, size_t pointsMin, size_t pointsMax, int lookBack) {
    std::vector<std::vector<PointT*>> clusters;
    std::array<std::vector<size_t>, nRings> ringsIndices {};
    std::array<std::array<PointT*, nRows>, nRings> ringsPoints {};

    PointCloudT::Ptr pcCopy = pc->makeShared();

    // Organize the point cloud in stacked rings
    for (size_t i = 0; i<pcCopy->size(); i++) {
        PointT* point = &pcCopy->points[i];
        const double range = sqrt(point->x*point->x + point->y*point->y + point->z*point->z);
        const size_t offset = (point->x >= 0) ? 563 : 1688;
        const size_t row = size_t(offset - asin(point->y / range) / 0.00279111);

        point->clusterId = 0; // temporarily (?) set the point cluster id to 0 as the deserialization can read garbage
        ringsPoints.at(point->ring).at(row) = point;
        ringsIndices.at(point->ring).push_back(row); // ToDo: reserve
    }

    // Main processing
    for(size_t ring=0; ring<nRings; ring++) {
        findClustersInRing(ringsIndices[ring], ringsPoints[ring], clusters, distanceMax, lookBack);
        propagateClustersBetweenRings(ringsIndices[ring], ringsPoints, clusters, ring, distanceMax);
    }

    // Make point clouds out of these clusters
    clustersOut.clear();
    clustersOut.reserve(clusters.size());
    for(std::vector<PointT*>& cluster : clusters) {
        if(cluster.size()>pointsMin && cluster.size()<pointsMax) {
            PointCloudT::Ptr pcCluster { new PointCloudT };
            pcCluster->reserve(cluster.size());

            for(PointT* p : cluster) {
                p->clusterId = clustersOut.size()+1;
                pcCluster->points.push_back(*p);
            }

            clustersOut.push_back(pcCluster);
        }
    }

    return true;
}

namespace tools
{

bool clustersToClustered(const std::vector<PointCloudT::Ptr>& clusters, PointCloudT::Ptr clustered) {
    for(PointCloudT::ConstPtr cluster : clusters) {
        *clustered += *cluster;
    }

    return true;
}

bool clusteredToClusters(PointCloudT::ConstPtr clustered, std::vector<PointCloudT::Ptr>& clusters) {
    clusters.clear(); clusters.reserve(256);
    std::unordered_map<uint16_t, size_t> mapIdCluster;

    for(const PointT& p : *clustered) {
        // Create a new cluster if not already exists

        if(mapIdCluster.find(p.clusterId) == mapIdCluster.end()) {
            mapIdCluster[p.clusterId] = clusters.size();
            clusters.emplace_back(new PointCloudT);
            clusters[mapIdCluster[p.clusterId]]->reserve(256);
        }

        clusters[mapIdCluster[p.clusterId]]->push_back(p);
    }

    return true;
}

bool clusteredToIndices(PointCloudT::ConstPtr clustered, std::vector<pcl::PointIndices::Ptr>& clustersIndices) {
    clustersIndices.clear(); clustersIndices.reserve(256);
    std::unordered_map<uint16_t, size_t> mapIdCluster;

    for(size_t i=0; i<clustered->size(); i++) {
        const PointT& p = clustered->at(i);
        // Create a new cluster if not already exists
        if(mapIdCluster.find(p.clusterId) == mapIdCluster.end()) {
            mapIdCluster[p.clusterId] = clustersIndices.size();
            clustersIndices.emplace_back(new pcl::PointIndices);
            clustersIndices[mapIdCluster[p.clusterId]]->indices.reserve(256);
        }

        clustersIndices[mapIdCluster[p.clusterId]]->indices.push_back(i);
    }

    return true;
}

} // namespace tools
} // namespace velodyne_utils
