#define PCL_NO_PRECOMPILE

#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/common/centroid.h>

#include "velodyne_utils/filtering.h"
#include "velodyne_utils/plane_fitting.h"

namespace velodyne_utils {

    using PointT = PointXYZIRC;
    using PointCloudT = pcl::PointCloud<PointT>;

    /*
     * Use a RANSAC model to fit a plane in a point cloud and extract corresponding points.
     * 
     * pc [in]: Input point cloud.
     * inliers [out, optionnal]: Points within the specified distance threshold to the plane. If `nullptr`, inliers won't be computed.
     * outliers [out, optionnal]: Points outside the specified distance threshold to the plane. If `nullptr`, outliers won't be computed.
     * maxDistToPlane [in]: Distance threshold between the plane and inlier points.
     * return: true if successful, false otherwise.
    */
    bool planeFitRansac(PointCloudT::ConstPtr pc, PointCloudT::Ptr inliers, PointCloudT::Ptr outliers, float maxDistToPlane) {
        if (!pc || pc->points.empty()) return false;

        pcl::ModelCoefficients::Ptr coefficients { new pcl::ModelCoefficients };
        pcl::PointIndices::Ptr inlierIndices { new pcl::PointIndices };

        // Create the segmentation object
        pcl::SACSegmentation<PointT> seg;
        seg.setModelType(pcl::SACMODEL_PLANE);
        seg.setMethodType(pcl::SAC_RANSAC);
        seg.setDistanceThreshold(maxDistToPlane);

        seg.setInputCloud(pc);
        seg.segment(*inlierIndices, *coefficients);

        // Extract the indices in the inlier and outlier point clouds
        pcl::ExtractIndices<PointT> extract;
        extract.setIndices(inlierIndices);
        extract.setInputCloud(pc);
        if(inliers)  { extract.setNegative(false); extract.filter(*inliers); };
        if(outliers) { extract.setNegative(true);  extract.filter(*outliers); };

        return true;
    }

    /*
     * Keep points close enough to the n lowest points. Sort of a coarse first plane estimation.
     * 
     * pc [in]: Input point cloud.
     * seeds [out]: Points close enough to the n lowest points.
     * nSeeds [in]: Number of points used to compute the low height mean.
     * distThresh [in]: Distance threshold between the n lowest points mean height and inlier points.
    */
    void pcaExtractInitialSeeds(const PointCloudT& pc, PointCloudT& seeds, size_t nSeeds, float distThresh) {
        // Extract the z components from the point cloud
        std::vector<float> heights; heights.reserve(pc.size());
        std::transform(pc.begin(), pc.end(), back_inserter(heights),
            [](const PointT& p) { return p.z; }
        );

        // Sort them and find the nSeeds lowest points' mean
        std::sort(heights.begin(), heights.end());

        nSeeds = std::min(nSeeds, heights.size());
        const float meanHeight = std::accumulate(heights.cbegin(), heights.cbegin()+nSeeds, 0.0) / nSeeds;

        // Output points as seeds if their distance to this mean is short enough
        seeds.reserve(pc.size());
        std::copy_if(pc.begin(), pc.end(), std::back_inserter(seeds.points),
            [meanHeight, distThresh](const PointT& p) { return std::abs(p.z-meanHeight)<distThresh; }
        );
    }

    /*
     * Estimate a plane parameters by PCA.
     * 
     * pc [in]: Input point cloud.
     * normal [out]: Plane normal (a, b, c parameters).
     * height [out]: Plane height (d parameter).
    */
    void pcaEstimatePlane(const PointCloudT& pc, Eigen::Vector3f& normal, float& height) {
        // Determine the mean and covariance of the point cloud
        Eigen::Matrix3f cov;
        Eigen::Vector4f pc_mean;
        pcl::computeMeanAndCovarianceMatrix(pc, cov, pc_mean);

        // Use the least singular vector of the SVD decomposition as normal
        const Eigen::JacobiSVD<Eigen::Matrix3f> svd { cov, Eigen::DecompositionOptions::ComputeFullU };
        normal = (svd.matrixU().col(2));

        // Mean inliers seeds value, such that d = -normal.T*[x,y,z]
        height = -(normal.transpose()*pc_mean.head<3>())(0,0);
    }

    /*
     * Use PCA to fit a plane in a point cloud and extract corresponding points.
     * 
     * pc [in]: Input point cloud.
     * inliers [out]: Points within the specified distance threshold to the plane.
     * outliers [out, optionnal]: Points outside the specified distance threshold to tpcl::PointXYZIRChe plane. If `nullptr`, outliers won't be computed.
     * maxDistToPlane [in]: Distance threshold between the plane and inlier points.
     * heightMin [in]: Lower height bound.
     * heightMax [in]: Upper height bound.
     * radiusMin [in]: Lower 2D distance bound.
     * radiusMax [in]: Upper 2D distance bound.
     * nSeeds [in]: Number of points used to compute the first coarse plane estimation.
     * nIter [in]: Number of plane fitting refinement iterations.
     * return: true if successful, false otherwise.
    */
    bool planeFitPca(PointCloudT::ConstPtr pc, PointCloudT::Ptr inliers, PointCloudT::Ptr outliers, float maxDistToPlane, float heightMin, float heightMax, float radiusMin, float radiusMax, size_t nSeeds, size_t nIter) {
        // Remove points outside the expected height slice
        PointCloudT::Ptr pcSliced { new PointCloudT };
        filterHeight(pc, pcSliced, nullptr, heightMin, heightMax);
        if(pcSliced->points.empty() || !inliers) return false;

        Eigen::Vector3f planeNormal;
        float planeHeight;

        // Initial coarse estimation of inliers points
        pcaExtractInitialSeeds(*pcSliced, *inliers, nSeeds, maxDistToPlane);

        // 5. Ground plane fitter mainloop
        for(size_t i=0; i<nIter; i++) {
            pcaEstimatePlane(*inliers, planeNormal, planeHeight);

            const Eigen::VectorXf  dists = pcSliced->getMatrixXfMap().topRows(3).transpose() * planeNormal;

            const float distThresh = maxDistToPlane - planeHeight;
            inliers->clear(); inliers->reserve(pcSliced->size());
            if(outliers) { outliers->clear(); outliers->reserve(pcSliced->size()); }

            for(size_t k=0; k<pcSliced->size(); k++) {
                const PointT& point = pcSliced->points[k];

                if(dists[k]<distThresh) {
                    inliers->push_back(point);
                }
                else if(!inRange(point, radiusMin) && inRange(point, radiusMax) && outliers) {
                    outliers->push_back(point);
                }
            }
        }

        return true;
    }

} // Namespace velodyne_utils
