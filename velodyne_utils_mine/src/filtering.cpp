#define PCL_NO_PRECOMPILE
#include <pcl/filters/statistical_outlier_removal.h>

#include "velodyne_utils/filtering.h"

namespace velodyne_utils {

    using PointT = PointXYZIRC;
    using PointCloudT = pcl::PointCloud<PointT>;

    /*
     * Filters a point cloud by height.
     * 
     * pc [in]: Input point cloud.
     * inliers [out, optionnal]: Points within the specified height range. If `nullptr`, inliers won't be computed.
     * outliers [out, optionnal]: Points outside the specified height range. If `nullptr`, outliers won't be computed.
     * zmin [in]: Lower height bound.
     * zmax [in]: Upper height bound.
     * return: true if successful, false otherwise.
    */
    bool filterHeight(PointCloudT::ConstPtr pc, PointCloudT::Ptr inliers, PointCloudT::Ptr outliers, float zmin, float zmax) {
        if(!pc || pc->points.empty()) return false;
        if(inliers)  inliers->reserve(pc->size());
        if(outliers) outliers->reserve(pc->size());

        for(size_t i = 0; i < pc->size(); ++i){
            const PointT& p = pc->points[i];
            if(p.z >= zmin && p.z < zmax) {
                if(inliers) inliers->points.push_back(p);
            }
            else {
                if(outliers) outliers->points.push_back(p);
            }
        }

        return true;
    }

    /*
     * Filters a point cloud by intensity.
     * 
     * pc [in]: Input point cloud.
     * inliers [out, optionnal]: Points within the specified intensity range. If `nullptr`, inliers won't be computed.
     * outliers [out, optionnal]: Points outside the specified intensity range. If `nullptr`, outliers won't be computed.
     * imin [in]: Lower intensity bound.
     * imax [in]: Upper intensity bound.
     * return: true if successful, false otherwise.
    */
    bool filterIntensity(PointCloudT::ConstPtr pc, PointCloudT::Ptr inliers, PointCloudT::Ptr outliers, float imin, float imax) {
        if(!pc || pc->points.empty()) return false;
        inliers->reserve(pc->size());
        if(outliers) outliers->reserve(pc->size());

        for(size_t i = 0; i < pc->size(); ++i) {
            const PointT& p = pc->points[i];
            if(p.intensity >= imin && p.intensity < imax) {
                if(inliers) inliers->points.push_back(p);
            }
            else {
                if(outliers) outliers->points.push_back(p);
            }
        }

        return true;
    }

    /*
     * Filters a point cloud by deviation to a mean.
     * 
     * pc [in]: Input point cloud.
     * inliers [out, optionnal]: Points within statistical range. If `nullptr`, inliers won't be computed.
     * outliers [out, optionnal]: Points outside statistical range. If `nullptr`, outliers won't be computed.
     * mean [in]: Statistical mean.
     * stddev [in]: Allowed standard deviation around the mean.
     * return: true if successful, false otherwise.
    */
    bool filterStatisticalOutlier(PointCloudT::ConstPtr pc, PointCloudT::Ptr inliers, PointCloudT::Ptr outliers, float mean, float stdDev) {
        if (!pc || pc->points.empty()) return false;

        pcl::StatisticalOutlierRemoval<PointT> extract;
        extract.setInputCloud(pc);
        extract.setMeanK(mean);
        extract.setStddevMulThresh(stdDev);
        if(inliers)  { extract.setNegative(false); extract.filter(*inliers); };
        if(outliers) { extract.setNegative(true);  extract.filter(*outliers); };

        return true;
    }

} // Namespace velodyne_utils

