#define PCL_NO_PRECOMPILE
#include <pcl/features/moment_of_inertia_estimation.h>
#include <pcl/common/transforms.h>
#include <pcl/common/common.h>
#include <pcl/common/pca.h>

#include <math.h>

#include "velodyne_utils/shape.h"
#include "velodyne_utils/l_shappe.h"

namespace velodyne_utils
{

using PointT = PointXYZIRC;
using PointCloudT = pcl::PointCloud<PointT>;
using Point2 = Eigen::Vector2f;
using Polygon2 = Eigen::Matrix<float, 2, Eigen::Dynamic>;
using Point3 = Eigen::Vector3f;
using Polygon3 = Eigen::Matrix<float, 3, Eigen::Dynamic>;

bool computeDelimitation(PointCloudT::ConstPtr pc, Eigen::Matrix<float, 2, Eigen::Dynamic>& delim, float heightMax, float distMin, float distMax, float zMax, float radialStep) {
    // Create and populate a radial array (where each cell is a radial slice around the sensor)
    // If a point is too high, it is considered ocluding what's behind
    Eigen::VectorXf distances = Eigen::VectorXf::Zero(std::round(std::floor(2*M_PI/radialStep)) + 2);

    for(const PointXYZIRC& point : pc->points) {
        const float distance = std::sqrt(point.x*point.x + point.y*point.y + point.z*point.z);
        const float azimuth = std::atan2(-point.y, -point.x)+M_PI;
        const Eigen::Index azimuth_i = std::round(std::ceil(azimuth/radialStep));

        if(azimuth_i<distances.rows() && azimuth_i>0 && distance>distMin && point.z<zMax) {
            // In which case, the information that this radial slice is ocluded is represented by
            // a number greater than the max allowed distance (later decoded with a modulus)
            // and it is the closest ocluding point that is kept
            if(point.z>heightMax && distance<std::fmod(distances[azimuth_i], distMax)) {
                distances[azimuth_i] = distance + distMax;
            }
            else if(distance>distances[azimuth_i] && distance<distMax) {
                distances[azimuth_i] = distance;
            }
        }
    }

    // Once the radial array is built, a polygon is constructed by computing 2D cartesian coordinates
    // from the polar angle+distance coordinates
    delim.resize(2, distances.rows());

    for(Eigen::Index i=0; i<distances.rows(); i++) {
        if(distances[i]==0)
            distances[i] = distMax-0.01;

        delim(0,i) = std::fmod(std::max(distances[i], distMin), distMax) * std::cos(i*radialStep);
        delim(1,i) = std::fmod(std::max(distances[i], distMin), distMax) * std::sin(i*radialStep);
    }

    return true;
}

Point3 calculPointIntersection(double a1, double b1, double c1, double a2,  double b2, double c2){
  Point3 point;

  double x = (c1*b2 - c2*b1)/(b2*a1 - b1*a2);
  double y = (c2*a1 - c1*a2)/(b2*a1 - b1*a2);

  point[0] = x;
  point[1] = y;
  point[2] = 0;

  return point;


}

bool computeBoundingBox(PointCloudT::ConstPtr clustered, jsk_recognition_msgs::BoundingBox& bbox) {

    LShappe lshappe;
    bbox = lshappe.lShappeComputing(clustered);

    // Apply a PCA on the point cloud main orientation
    /*pcl::PCA<PointT> pca; pca.setInputCloud(clustered);
    Point3 orientation = pca.getEigenVectors().col(0);
    orientation[2] = 0;*/

    // Rotate the point cloud by that orientation - place the points in the rectangle frame
    /*Eigen::Quaternionf rot; rot.setFromTwoVectors(orientation, Point3(1,0,0));
    Eigen::Affine3f transform; transform = rot.toRotationMatrix();
    PointCloudT transformed; pcl::transformPointCloud(*clustered, transformed, transform);*/

    // "Fit" a rectangle by taking the min/max in x and y and determine its length and width
    /*const double minx = transformed.getMatrixXfMap().row(0).minCoeff(), maxx = transformed.getMatrixXfMap().row(0).maxCoeff();
    const double miny = transformed.getMatrixXfMap().row(1).minCoeff(), maxy = transformed.getMatrixXfMap().row(1).maxCoeff();
    const double minz = transformed.getMatrixXfMap().row(2).minCoeff(), maxz = transformed.getMatrixXfMap().row(2).maxCoeff();*/

    // Apply the inverse transformation to get back in the original frame
    //Point3 orig;
    //pcl::transformPoint(Point3((maxx+minx)/2, (maxy+miny)/2, (minz+maxz)/2), orig, transform.inverse());

    /*bbox.pose.position.x = orig[0];
    bbox.pose.position.y = orig[1];
    bbox.pose.position.z = orig[2];
    bbox.pose.orientation.x = rot.x();
    bbox.pose.orientation.y = rot.y();
    bbox.pose.orientation.z = rot.z();
    bbox.pose.orientation.w = -rot.w();
    bbox.dimensions.x = maxx-minx;
    bbox.dimensions.y = maxy-miny;
    bbox.dimensions.z = maxz-minz;*/
    return true;
}

inline float cross(const Point2& O, const Point2& A, const Point2& B) {
    return (A[0] - O[0]) * (B[1] - O[1]) - (A[1] - O[1]) * (B[0] - O[0]);
}

bool lexicographicalSort(const Polygon2& in, Polygon2& out) {
    std::vector<Point2> vec;

    // Unfortunately sorting algorithms are not available on eigen matrices
    //so we have to transform to std vector and back
    for (Eigen::Index i = 0; i < in.cols(); i++)
        vec.push_back(in.col(i));

    std::sort(vec.begin(), vec.end(), [](const Point2& A, const Point2& B)
    {
        return A[0] < B[0] || (A[0] == B[0] && A[1] < B[1]);
    });

    out.resize(Eigen::NoChange, in.cols());
    for (Eigen::Index i = 0; i < in.cols(); i++)
        out.col(i) = vec[i];

    return true;
}

bool computeHull(const Polygon2& P, Polygon2& H) {
    Eigen::Index n = P.cols(), i = 0, t = 0, m = 0;

    if (n > 3) {
        Polygon2 Psorted;
        lexicographicalSort(P, Psorted);

        Polygon2 hull(2, 2*Psorted.cols());

        // Build lower hull
        for (i = 0; i < n; ++i) {
            while (m >= 2 && cross(hull.col(m - 2), hull.col(m - 1), hull.col(i)) <= 0)
                m--;

            hull.col(m++) = Psorted.col(i);
        }

        // Build upper hull
        for (i = n - 1, t = m + 1; i > 0; --i) {
            while (m >= t && cross(hull.col(m - 2), hull.col(m - 1), Psorted.col(i - 1)) <= 0)
                m--;

            hull.col(m++) = Psorted.col(i - 1);
        }

        H = hull.leftCols(m);
    }
    else {
        H = P.leftCols(n);
    }

    return true;
}

bool computeHull(PointCloudT::ConstPtr pc, geometry_msgs::Polygon& hull, float z) {
    Polygon2 H;
    computeHull(pc->getMatrixXfMap().topRows<2>(), H);

    hull.points.clear(); hull.points.resize(H.cols());
    for(Eigen::Index i=0; i<H.cols(); i++) {
        hull.points[i].x = H(0,H.cols()-i-1);
        hull.points[i].y = H(1,H.cols()-i-1);
        hull.points[i].z = z;
    }

    return true;
}

bool computeDescriptors(pcl::PointCloud<PointXYZIRC>::ConstPtr pc, velodyne_utils::PolygonWithDescriptors& polygon) {
	// Check if polygon is known to compute its area (HYPO: convex polygon)
	if (polygon.polygon.points.size() > 0) {
		//float area = 0;
		Eigen::MatrixXf points(polygon.polygon.points.size()+1, 2);
		for (int i = 0; i < (int) polygon.polygon.points.size(); ++i) {
			points.row(i) << polygon.polygon.points[i].x, polygon.polygon.points[i].y;
		}
		points.row(polygon.polygon.points.size()) << polygon.polygon.points[0].x, polygon.polygon.points[0].y;
		Descriptor descriptorArea;
		descriptorArea.name = "area";
		descriptorArea.value = std::fabs(0.5 * (points.col(0).head(polygon.polygon.points.size()).dot(points.col(1).tail(polygon.polygon.points.size()))-points.col(0).tail(polygon.polygon.points.size()).dot(points.col(1).head(polygon.polygon.points.size()))));
		polygon.descriptors.push_back(descriptorArea);
	}

	Descriptor descriptor;
	descriptor.name = "size";
	descriptor.value = pc->size();
	polygon.descriptors.push_back(descriptor);

	auto points = pc->getMatrixXfMap();
	// Compute min, max, mean intensity
	descriptor.name = "intensity_min";
	descriptor.value = points.row(4).minCoeff();
	polygon.descriptors.push_back(descriptor);
	descriptor.name = "intensity_max";
	descriptor.value = points.row(4).maxCoeff();
	polygon.descriptors.push_back(descriptor);
	descriptor.name = "intensity_mean";
	descriptor.value = points.row(4).mean();
	polygon.descriptors.push_back(descriptor);

	// Center (mean)
	descriptor.name = "x_mean";
	descriptor.value = points.row(0).mean();
	polygon.descriptors.push_back(descriptor);
	descriptor.name = "y_mean";
	descriptor.value = points.row(1).mean();
	polygon.descriptors.push_back(descriptor);
	descriptor.name = "z_mean";
	descriptor.value = points.row(2).mean();
	polygon.descriptors.push_back(descriptor);

	// Center (median)
	//descriptor.name = "x_median";
	//descriptor.value = points.row(0).median();
	//polygon.descriptors.push_back(descriptor);
	//descriptor.name = "y_median";
	//descriptor.value = points.row(1).median();
	//polygon.descriptors.push_back(descriptor);
	//descriptor.name = "z_median";
	//descriptor.value = points.row(2).median();
	//polygon.descriptors.push_back(descriptor);

	// eigen values descriptors
	pcl::PCA<PointT> pca; pca.setInputCloud(pc);
	float l1 = pca.getEigenValues().x();
	float l2 = pca.getEigenValues().y();
	float l3 = pca.getEigenValues().z();
	float e1 = l1 / (l1+l2+l3);
	float e2 = l2 / (l1+l2+l3);
	float e3 = l3 / (l1+l2+l3);
	descriptor.name = "linearity";
	descriptor.value = (e1-e2)/e1;
	polygon.descriptors.push_back(descriptor);
	descriptor.name = "planarity";
	descriptor.value = (e2-e3)/e1;
	polygon.descriptors.push_back(descriptor);
	descriptor.name = "scattering";
	descriptor.value = e3/e1;
	polygon.descriptors.push_back(descriptor);
	descriptor.name = "omnivariance";
	descriptor.value = std::cbrt(e1*e2*e3);
	polygon.descriptors.push_back(descriptor);
	descriptor.name = "anisotropy";
	descriptor.value = (e1-e3)/e1;
	polygon.descriptors.push_back(descriptor);
	descriptor.name = "eigenentropy";
	descriptor.value = -e1*log(e1)-e2*log(e2)-e3*log(e3);
	polygon.descriptors.push_back(descriptor);
	descriptor.name = "sum_of_eigenvalues";
	descriptor.value = l1+l2+l3;
	polygon.descriptors.push_back(descriptor);
	descriptor.name = "change_of_curvature";
	descriptor.value = e3/(e1+e2+e3);
	polygon.descriptors.push_back(descriptor);


	return true;
}

namespace tools
{

bool pointcloudToPolygon(const PointCloudT& pc, geometry_msgs::Polygon& hull, float z) {
    hull.points.clear(); hull.points.resize(pc.size());
    for(size_t i=0; i<pc.size(); i++) {
        hull.points[i].x = pc.at(i).x;
        hull.points[i].y = pc.at(i).y;
        hull.points[i].z = z;
    }

    return true;
}

}

} // namespace velodyne_utils
