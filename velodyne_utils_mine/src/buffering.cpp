#define PCL_NO_PRECOMPILE

#include "velodyne_utils/point_types.h"

#include <tf/transform_listener.h>

#include <pcl/filters/extract_indices.h>
#include <pcl_ros/point_cloud.h>
#include <pcl_ros/transforms.h>
#include "pcl_ros/impl/transforms.hpp"
#include <pcl_conversions/pcl_conversions.h>

#include <geometry_msgs/TwistWithCovarianceStamped.h>

#include "velodyne_utils/buffering.h"

namespace velodyne_utils {

RollingBuffer::RollingBuffer()
        : RollingBuffer::RollingBuffer("base", 1) {}

RollingBuffer::RollingBuffer(const std::string& frame_id, double length)
        : _frame_id(frame_id), _length(length), _megaSize(0), _tfListener(), _megaPointCloud(new pcl::PointCloud<PointXYZIRC>()) {}

    void RollingBuffer::addTwist(const geometry_msgs::TwistWithCovarianceStamped::ConstPtr& twistMsg) {
        // Add new twist
        _twistBuffer.push_back(twistMsg);

        // Remove old twists
        double now = ros::Time::now().toSec();
        while(_twistBuffer[0]->header.stamp.toSec() < now-_length && _twistBuffer.size() > 0) {
            _twistBuffer.pop_front();
        }
    }

    void RollingBuffer::addPointCloud(pcl::PointCloud<PointXYZIRC>::ConstPtr pc) {
        double now = ros::Time::now().toSec();

        // Remove old points
        const clock_t start1 = clock();
        int delIdx = 0;
        pcl::PointIndices::Ptr oldIndices(new pcl::PointIndices());
        while(_pcTimes.size() > 0 && _pcTimes[0] < now-_length) {
            for(int i = 0; i < _megaPointCloud->size(); ++i) {
                if ((*_megaPointCloud)[i].clusterId == delIdx) {
                    oldIndices->indices.push_back(i);
                }
            }
            ++delIdx;
            _pcTimes.pop_front();
        }
        std::cout << "Removing old points:      " << 1000.0 * double(clock()-start1) / double(CLOCKS_PER_SEC) << '\n';
        if (oldIndices->indices.size() > 0) {
            pcl::ExtractIndices<PointXYZIRC> extract;
            extract.setInputCloud(_megaPointCloud);
            extract.setIndices(oldIndices);
            extract.setNegative(true);
            extract.filter(*_megaPointCloud);
        }
        std::cout << "Removing old points:      " << 1000.0 * double(clock()-start1) / double(CLOCKS_PER_SEC) << '\n';

        // Shift pointcloud index (stored in the clusterid)
        const clock_t start2 = clock();
        for(auto& point: *_megaPointCloud) {
            point.clusterId -= delIdx;
        }
        std::cout << "Shifting point indexes:   " << 1000.0 * double(clock()-start2) / double(CLOCKS_PER_SEC) << '\n';

        // Transform mega point cloud in new frame
        const clock_t start3 = clock();
        tf::Transform transform = getTransform(_megaPointCloud->header.stamp, pc->header.stamp);
        std::cout << "Get transform:            " << 1000.0 * double(clock()-start3) / double(CLOCKS_PER_SEC) << '\n';
        const clock_t start4 = clock();
        pcl_ros::transformPointCloud<PointXYZIRC>(*_megaPointCloud, *_megaPointCloud, transform.inverse());
        std::cout << "Trans megaPointCloud:     " << 1000.0 * double(clock()-start4) / double(CLOCKS_PER_SEC) << '\n';

        // Move new point cloud to _frame_id
        const clock_t start5 = clock();
        pcl::PointCloud<PointXYZIRC> newPc;
        pcl_ros::transformPointCloud(_frame_id, *pc, newPc, _tfListener);
        std::cout << "Transform newPc:          " << 1000.0 * double(clock()-start5) / double(CLOCKS_PER_SEC) << '\n';

        // Set new pointcloud id
        const clock_t start6 = clock();
        for(auto& point : newPc) {
            point.clusterId = _megaSize-delIdx;
        }
        std::cout << "Set pointcloud id:        " << 1000.0 * double(clock()-start6) / double(CLOCKS_PER_SEC) << '\n';

        // Add new pointcloud to megapointcloud
        const clock_t start7 = clock();
        *_megaPointCloud += newPc;
        _megaPointCloud->header.stamp = pc->header.stamp;
        _megaPointCloud->header.frame_id = _frame_id;
        std::cout << "Concat pointclouds:       " << 1000.0 * double(clock()-start6) / double(CLOCKS_PER_SEC) << '\n';

        // Update member variables
        _megaSize += 1-delIdx;
        _pcTimes.push_back(pc->header.stamp*1e-6);
    }

    tf::Transform RollingBuffer::getTransform(uint64_t start, uint64_t end) {
        tf::Transform transform;
        tf::Transform dtransform;

        // Empty twist buffer
        if (_twistBuffer.empty()) return transform;

        // Build transform
        int i = 0;
        while (i+1 < _twistBuffer.size() && _twistBuffer[i]->header.stamp.toSec() < start*1e-6) {
            ++i;
        }
        double dt = _twistBuffer[i]->header.stamp.toSec() - start*1e-6;
std::cout << dt << '\n';
        double speed = _twistBuffer[i]->twist.twist.linear.x;
        double yawRate = _twistBuffer[i]->twist.twist.angular.z;
        transform.setOrigin(tf::Vector3(
            dt*speed*cos(yawRate*dt/2),
            dt*speed*sin(yawRate*dt/2),
            0
        ));
        tf::Quaternion q(tf::Vector3(0,0,1), yawRate*dt);
        transform.setRotation(q);
        while(i+1 < _twistBuffer.size() && _twistBuffer[i+1]->header.stamp.toSec() < end*1e-6) {
            dt = (_twistBuffer[i+1]->header.stamp - _twistBuffer[i]->header.stamp).toSec();
std::cout << dt << '\n';
            speed = _twistBuffer[i]->twist.twist.linear.x;
            yawRate = _twistBuffer[i]->twist.twist.angular.z;

            dtransform.setOrigin(tf::Vector3(
                dt*speed*cos(yawRate*dt/2),
                dt*speed*sin(yawRate*dt/2),
                0
            ));
            q.setRotation(tf::Vector3(0,0,1), yawRate*dt);
            dtransform.setRotation(q);
            transform *= dtransform;

            ++i;
        }

        dt = end*1e-6 - _twistBuffer[i]->header.stamp.toSec();
std::cout << dt << '\n';
        speed = _twistBuffer[i]->twist.twist.linear.x;
        yawRate = _twistBuffer[i]->twist.twist.angular.z;

        dtransform.setOrigin(tf::Vector3(
            dt*speed*cos(yawRate*dt/2),
            dt*speed*sin(yawRate*dt/2),
            0
        ));
        q.setRotation(tf::Vector3(0,0,1), yawRate*dt);
        dtransform.setRotation(q);
        transform *= dtransform;

        return transform;
    }

}
