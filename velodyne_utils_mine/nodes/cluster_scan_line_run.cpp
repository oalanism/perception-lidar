#include <ros/ros.h>
#include <nodelet/loader.h>

int main(int argc, char* argv[])
{
    ros::init(argc, argv, "cluster_scan_line_run");
    nodelet::Loader nodelet;
    nodelet.load(ros::this_node::getName(), "velodyne_utils/ClusterScanLineRunNodelet", ros::names::getRemappings(), nodelet::V_string());
    ros::spin();
    return 0;
}
