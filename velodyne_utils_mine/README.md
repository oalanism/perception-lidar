# Velodyne Utils Package

This package contains several nodelet that can be used to process lidar 
pointcouds.

Each program derives from the class `Filter` which subscribes and publishes 
PointCloud2 messages. Thus they can be chained to perform complex processing.

An example launch file is provided.

## Filter Intensity

### Description

This nodelet filters PointCloud2 messages based on the intensity points.

### Parameters

* `intensity_min`: Minimum intensity allowed (inclusive)
* `intensity_max`: Maximum intensity allowed (exclusive)

## Filter Cluster

### Description

This nodelet extract clusters from a PointCloud2. It publishes a single 
PointCloud2 with the addition of the field clusterId (last 2 bits of the 32bits
 description of Lidar points). The clustering is performed by a Euclidean 
Distance approach.

This nodelet also perform feature extraction for each cluster. In particular 
the bounding boxes of each cluster are extracted. Markers are published to 
display the bounding boxes and the 2D centroid of each cluster.

### Parameters

* `cluster_max_distance`: Maximum distance allowed between a point and the 
other points of the cluster.
* `cluster_min_points`: Minimum number of points in a cluster.

---

## Delimiter

### Description

This node computes perception areas for given point clouds. The perception area is the geometrical zone where the sensor have measured points. This area is arbitrary limited by a minimal and maximal radius, as measurements outside this disc can be considered too noisy. Tall object can also block the perception behind them and a naive approach to this is to ignore every point that is behing such a tall object. This computation is done with the help of a polar grid, whose radial step can be specified. The output of the node is a geometry_msgs/PolygonStamped whose header is identical to the input cloud.

### Parameters

* `topic_in`: Input PointCloud2 topic
* `topic_out`: Output Polygon topic
* `radial_step`: Step of the radial grid, in rad
* `dist_min`: Minimal distance for points to be taken into account, in meters (to avoid roof hits)
* `dist_max`: Maximal distance for points to be taken into account, in meters (to avoid distant noise)
* `height_max`: Height for points to be considered obstacles occluding their background, in meters