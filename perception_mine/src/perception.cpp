#include <ros/ros.h>
#include <perception/cluster.h>
#include <perception/model_stimation.h>
#include <jsk_recognition_msgs/BoundingBoxArray.h>
#include "perception/BoxesArray.h"
#include "perception/BoxMessage.h"
#include <perception/perception_diagnostic.h>

ros::Time begin;
ros::Publisher pub_boxes_;
ros::Publisher pub_bboxes_;

std::vector<perception::Cluster> Clusters;
perception::ModelStimation ms;
jsk_recognition_msgs::BoundingBoxArray::Ptr bboxes { new jsk_recognition_msgs::BoundingBoxArray };
perception::BoxesArray boxes;

Diag_com diag;

bool clusteredToClusters(perception::PointCloudT::ConstPtr clustered, std::vector<perception::PointCloudT::Ptr>& clusters) {
  clusters.clear(); clusters.reserve(256);

  std::unordered_map<uint16_t, size_t> mapIdCluster;

  for(const perception::PointT& p : *clustered) {
      // Create a new cluster if not already exists

      if(mapIdCluster.find(p.clusterId) == mapIdCluster.end()) {
          mapIdCluster[p.clusterId] = clusters.size();
          clusters.emplace_back(new perception::PointCloudT);
          clusters[mapIdCluster[p.clusterId]]->reserve(256);
      }

      clusters[mapIdCluster[p.clusterId]]->push_back(p);
  }

  return true;
}


void peceptionCallback(perception::PointCloudT::ConstPtr pc)
{
  ros::Time now = ros::Time::now();
  ros::Duration d(0.2);

  if(now < begin){ begin = now; Clusters.clear();}

  std::vector<std::pair<int, int> > pairs;
  std::vector<bool> Ccmatches(Clusters.size(), false);


  if((now - begin) > d){
    begin = now;
    std::vector<perception::PointCloudT::Ptr> clusters;

    clusteredToClusters(pc, clusters);

    std::vector<bool> Pcmatches(clusters.size(), false);

    bboxes->header = pcl_conversions::fromPCL(pc->header);
    bboxes->boxes.resize(clusters.size());

    double euclidean[clusters.size()][Clusters.size()];

    for(size_t i = 0; i < clusters.size(); i++) {

      double mean_x = 0, mean_y = 0, mean_z = 0;
      float euclidean_distance = 0.5;

      unsigned int position;

      mean_x = (clusters[i]->getMatrixXfMap().row(0).sum())/(clusters[i]->getMatrixXfMap().rows());
      mean_y = (clusters[i]->getMatrixXfMap().row(1).sum())/(clusters[i]->getMatrixXfMap().rows());
      mean_z = (clusters[i]->getMatrixXfMap().row(2).sum())/(clusters[i]->getMatrixXfMap().rows());
      float min_distance = euclidean_distance;
      for (size_t j = 0; j < Clusters.size(); j++) {
        euclidean[i][j] = abs(mean_x - Clusters[j].meanX()) + abs(mean_y - Clusters[j].meanY()) + abs(mean_z - Clusters[j].meanZ());
        if(min_distance > euclidean[i][j]){
          position = j;
          min_distance = euclidean[i][j];
        }
      }

      if(min_distance < euclidean_distance){
        Clusters[position].update(clusters[i]);
        Pcmatches[i] = true; Ccmatches[position] = true;
      }
    }

    int idLast;

    if(Clusters.size() > 0) idLast = Clusters[Clusters.size()-1].Id();
    else idLast = -1;

    for (size_t i = 0; i < Ccmatches.size(); i++)
      if(!Ccmatches[i]) Clusters.erase(Clusters.begin() +i );

    for (size_t i = 0; i < Pcmatches.size(); i++) {
      if(!Pcmatches[i]){

        pcl::PointCloud<pcl::PointXYZ>::Ptr testing_cloud (new pcl::PointCloud<pcl::PointXYZ> );
        testing_cloud->reserve(256);

        testing_cloud->width = clusters[i]->width;
        testing_cloud->height = clusters[i]->height;
        testing_cloud->resize(testing_cloud->width * testing_cloud->height);

        testing_cloud->getMatrixXfMap().row(0) = clusters[i]->getMatrixXfMap().row(0);
        testing_cloud->getMatrixXfMap().row(1) = clusters[i]->getMatrixXfMap().row(1);
        testing_cloud->getMatrixXfMap().row(2) = clusters[i]->getMatrixXfMap().row(2);

        int cn = ms.detectObject(testing_cloud);
        perception::Cluster c;
        c.cluster(idLast+1, clusters[i], cn);
        Clusters.push_back(c);
        idLast++;
      }
    }

    for (size_t i = 0; i < Clusters.size(); i++) {



      bboxes->boxes[i] = Clusters[i].Bbox();
      bboxes->boxes[i].label = Clusters[i].Id();
      bboxes->boxes[i].header = bboxes->header;

      perception::BoxMessage b;
      b.centerX = bboxes->boxes[i].pose.position.x;
      b.centerY = bboxes->boxes[i].pose.position.y;
      b.centerZ = bboxes->boxes[i].pose.position.z;
      b.orientationX = bboxes->boxes[i].pose.orientation.x;
      b.orientationY = bboxes->boxes[i].pose.orientation.y;
      b.orientationZ = bboxes->boxes[i].pose.orientation.z;
      b.orientationW = bboxes->boxes[i].pose.orientation.w;
      b.dimensionX = bboxes->boxes[i].dimensions.x;
      b.dimensionY = bboxes->boxes[i].dimensions.y;
      b.dimensionZ = bboxes->boxes[i].dimensions.z;
      b.label = Clusters[i].Label();
      b.velocity = Clusters[i].Velocity();
      boxes.Boxes.push_back(b);
    }
    pub_boxes_.publish(boxes);
    pub_bboxes_.publish(bboxes);
    //std::cin >> cn;
  }

  sy27_diagnostic_msgs::Status message;
  message.level = sy27_diagnostic_msgs::Status::STATUS_OK;
  diag.status_publisher.publish(message);
}

int main(int argc, char **argv){
  ros::init(argc, argv, "perception");
  diag.onInit(argc, argv);

  sy27_diagnostic_msgs::Status message;
  message.level = sy27_diagnostic_msgs::Status::STATUS_STARTING;
  diag.status_publisher.publish(message);



  ros::NodeHandle n;

  begin = ros::Time::now();

  ros::Subscriber sub_points_ = n.subscribe("vlp32C/object_points", 10, peceptionCallback);

  pub_bboxes_ = n.advertise<jsk_recognition_msgs::BoundingBoxArray>("bboxes", 10);
  pub_boxes_ = n.advertise<perception::BoxesArray>("boxes", 10);

  ros::spin();

  return 0;
}
