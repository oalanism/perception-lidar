#include <jsk_recognition_msgs/BoundingBox.h>
#include "perception/point_types.h"
#include <math.h>
#include <Eigen/Dense>
#include <perception/kalman.h>
#define PI 3.14159265

namespace perception
{
  using Point3 = Eigen::Vector3f;
  using PointT = PointXYZIRC;
  using PointCloudT = pcl::PointCloud<PointT>;
  using BoundingBox = jsk_recognition_msgs::BoundingBox;


  class LShappe{
    private:
      BoundingBox bbox;
      double a1, b1, c1, a2, b2, c2, a3, b3, c3, a4, b4, c4;
      double theta_;
      double velocity;
      int label;
      Kalman kalman;
    public:
      Point3 calculPointIntersection(double a1, double b1, double c1, double a2,  double b2, double c2);
      double closenessCriterion(Eigen::MatrixXf D1D2, float d0);
      BoundingBox Bbox(){return bbox;};
      double orientation() {return theta_;};
      double Velocity() {return velocity;};
      void setBbox(Point3 point, double rx, double ry, double rz, double rw, double dx, double dy, double dz);
      BoundingBox lShappeComputing(PointCloudT::ConstPtr clustered);
      void initKalman(float x, float y, float theta, float v);
      void updateKalman(float x, float y, float theta);
  };

  void LShappe::initKalman(float x, float y, float theta, float v){

      kalman.kalman(x, y, theta, v);
      velocity = kalman.velocity();

  }

  void LShappe::updateKalman(float x, float y, float theta){
    kalman.update(x, y, theta);
    velocity = kalman.velocity();
  }

  Point3 LShappe::calculPointIntersection(double a1, double b1, double c1, double a2,  double b2, double c2){
    Point3 point;

    double x = (c1*b2 - c2*b1)/(b2*a1 - b1*a2);
    double y = (c2*a1 - c1*a2)/(b2*a1 - b1*a2);

    point[0] = x;
    point[1] = y;
    point[2] = 0;

    return point;


  }

  double LShappe::closenessCriterion(Eigen::MatrixXf D1D2, float d0){
    float alpha = 0;
    for(int i = 0; i < D1D2.cols(); i++){
      float min = D1D2.col(i).minCoeff();
      float d;
      if(d0 > min) d = d0;
      else d = min;

      alpha = alpha + 1/d;

    }
    return alpha;
  }

  void LShappe::setBbox(Point3 point, double rx, double ry, double rz, double rw, double dx, double dy, double dz){
    bbox.pose.position.x = point[0];
    bbox.pose.position.y = point[1];
    bbox.pose.position.z = point[2];
    bbox.pose.orientation.x = rx;
    bbox.pose.orientation.y = ry;
    bbox.pose.orientation.z = rz;
    bbox.pose.orientation.w = rw;
    bbox.dimensions.x = dx;
    bbox.dimensions.y = dy;
    bbox.dimensions.z = dz;

    return;
  }

  BoundingBox LShappe::lShappeComputing(PointCloudT::ConstPtr clustered){
    float step = PI/32;

    double qMax;

    const double minz = clustered->getMatrixXfMap().row(2).minCoeff(), maxz = clustered->getMatrixXfMap().row(2).maxCoeff();
    for(float theta = 0; theta < ((PI/2) - step); theta = theta + step)
    {

      Eigen::VectorXf px(clustered->getMatrixXfMap().cols());
      px = clustered->getMatrixXfMap().row(0);

      Eigen::VectorXf py(clustered->getMatrixXfMap().cols());
      py = clustered->getMatrixXfMap().row(1);

      Eigen::VectorXf proyection_x(clustered->getMatrixXfMap().cols());
      Eigen::VectorXf proyection_y(clustered->getMatrixXfMap().cols());

      proyection_x = cos(theta)*px + sin(theta)*py;
      proyection_y = -sin(theta)*px + cos(theta)*py;


      double e1Min = proyection_x.minCoeff(), e1Max = proyection_x.maxCoeff();
      double e2Min = proyection_y.minCoeff(), e2Max = proyection_y.maxCoeff();

      //float alpha = -(e2Max - e2Min) * (e1Max - e1Min);

      Eigen::VectorXf e1MinVector = Eigen::VectorXf::Constant(clustered->getMatrixXfMap().cols(), e1Min);
      Eigen::VectorXf e1MaxVector = Eigen::VectorXf::Constant(clustered->getMatrixXfMap().cols(), e1Max);
      Eigen::VectorXf e2MinVector = Eigen::VectorXf::Constant(clustered->getMatrixXfMap().cols(), e2Min);
      Eigen::VectorXf e2MaxVector = Eigen::VectorXf::Constant(clustered->getMatrixXfMap().cols(), e2Max);

      Eigen::MatrixXf D1D2(4, clustered->getMatrixXfMap().cols());
      D1D2.row(0) = proyection_x - e1MinVector; D1D2.row(0) = D1D2.row(0).cwiseAbs();
      D1D2.row(1) = e1MaxVector - proyection_x; D1D2.row(1) = D1D2.row(1).cwiseAbs();
      D1D2.row(3) = proyection_y - e2MinVector; D1D2.row(2) = D1D2.row(2).cwiseAbs();
      D1D2.row(2) = e2MaxVector - proyection_y; D1D2.row(3) = D1D2.row(3).cwiseAbs();

      float d0 = 5e-5;
      float alpha = closenessCriterion(D1D2, d0);

      if(theta == 0 || qMax < alpha){
        theta_ = theta;
        qMax = alpha;
        c1 = e1Min;
        c2 = e2Min;
        c3 = e1Max;
        c4 = e2Max;
      }

    }

    a1 = cos(theta_);
    b1 = sin(theta_);
    a2 = -sin(theta_);
    b2 = cos(theta_);
    a3 = cos(theta_);
    b3 = sin(theta_);
    a4 = -sin(theta_);
    b4 = cos(theta_);

    std::vector<Point3> corners;

    corners.push_back(calculPointIntersection(a1, b1, c1, a2, b2, c2));//p1
    corners.push_back(calculPointIntersection(a2, b2, c2, a3, b3, c3));//p3
    corners.push_back(calculPointIntersection(a4, b4, c4, a3, b3, c3));//p4
    corners.push_back(calculPointIntersection(a1, b1, c1, a4, b4, c4));//p2

    float min_distance = sqrt(pow(corners[0][0], 2) + pow(corners[0][1],2));
    int id = 0;

    for (size_t i = 1; i < 4; i++) {
      double d = sqrt(pow(corners[i][0], 2) + pow(corners[i][1], 2));

      if(d < min_distance)
        id = i;
    }

    int id_s;
    int id_p;

    id_s = (id == 3) ? 0 : id+1;
    id_p = (id == 0) ? 3 : id-1;

    float theta_boite_1 = atan((corners[id_s][1] - corners[id][1])/(corners[id_s][0] - corners[id][0]));
    float theta_boite_2 = atan((corners[id_p][1] - corners[id][1])/(corners[id_p][0] - corners[id][0]));


    // Apply the inverse transformation to get back in the original frame
    Point3 orig;
    orig[0] = (corners[0][0]+corners[2][0])/2;
    orig[1] = (corners[1][1]+corners[3][1])/2;
    orig[2] = (maxz+minz)/2;


    double L1 = c3 - c1;
    double L2 = c4 - c2;
    double L3 = maxz - minz;

    setBbox(orig, 0, 0, sin(theta_/2), cos(theta_/2), L1, L2, L3);

    return Bbox();
  }

}
