#ifndef DIAG_COM_H
#define DIAG_COM_H

#include "ros/ros.h"
#include "sy27_diagnostic_msgs/Status.h"
#include "sy27_diagnostic_msgs/Diag_info.h"
#include "std_msgs/Time.h"
#include "diagnostic_msgs/KeyValue.h"
#include <string>

class Diag_com{
 public:
  void onInit(int argc, char **argv);
  ~Diag_com();

  ros::Publisher  status_publisher;


 private:
  void info_receive_callback(const sy27_diagnostic_msgs::Diag_info s);
  void watchdog_callback(const ros::TimerEvent& e);


  ros::Subscriber info_receiver;
  ros::Timer      diag_watchdog;

};

void Diag_com::onInit(int argc, char **argv){

  ros::init(argc, argv, "diag_perception"); // YOU MUST CHANGE THE NAME FOR SMTH UNIQUE

  ros::NodeHandle node_handle;

  info_receiver     = node_handle.subscribe("/diagnostic/broadcast_info", 100, &Diag_com::info_receive_callback, this);
  diag_watchdog     = node_handle.createTimer(ros::Duration(2.0), &Diag_com::watchdog_callback, this);
  status_publisher  = node_handle.advertise<sy27_diagnostic_msgs::Status>("/diagnostic/report_status", 100);

  ROS_INFO("diagnostic communication node started !");

}

Diag_com::~Diag_com(){



}

void Diag_com::info_receive_callback(const sy27_diagnostic_msgs::Diag_info s){
  diag_watchdog.setPeriod(ros::Duration(2.0), true);

  sy27_diagnostic_msgs::Status answer;
  answer.id = sy27_diagnostic_msgs::Status::ID_PERCEPTION; // replace ID_COMMUNICATION with your id name
  answer.header.stamp = ros::Time::now();

  switch(s.code){
    case sy27_diagnostic_msgs::Diag_info::ASK_REPORT:
      //Do what you have to do when you received the report status message
      answer.level = sy27_diagnostic_msgs::Status::STATUS_OK; // for example

      /* Don't forget to use also the "message" and the "keyvalue" member of the
       *  Status message structure. You can send informations to display thanks to that.
       */
      answer.keyvalue[0].key = std::string("log1"); //Examples
      answer.keyvalue[0].value = std::string("msg1");
      answer.keyvalue[1].key = std::string("log2");
      answer.keyvalue[1].value = std::string("msg2");

      ROS_INFO("STATUS_OK msg send");
      status_publisher.publish(answer);
      break;

      /*
       * Repeat the same pattern that the case just before with other kind of code
       * sy27_diagnostic_msgs::Diag_info::KO_NAVIGATION for example
       */

    default:
      // You can remove this part if you want
      ROS_INFO("Uninteresting code received : %u", s.code);
  }

}

void Diag_com::watchdog_callback(const ros::TimerEvent& e){
  sy27_diagnostic_msgs::Status message;
  message.level = sy27_diagnostic_msgs::Status::STATUS_OK;
  status_publisher.publish(message);
  ROS_INFO("Watchdog trigger");
}



#endif
