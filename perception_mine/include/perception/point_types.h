#ifndef SRC_POINT_TYPES_H
#define SRC_POINT_TYPES_H

#include <pcl_ros/point_cloud.h>

namespace perception {
// Inspired by velodyne_pointcloud::PointXYZIR
struct EIGEN_ALIGN16 PointXYZIRC{
    PCL_ADD_POINT4D
    float intensity;
    uint16_t ring;
    uint16_t clusterId;
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

}

POINT_CLOUD_REGISTER_POINT_STRUCT(perception::PointXYZIRC,
                                  (float, x, x)
                                  (float, y, y)
                                  (float, z, z)
                                  (float, intensity, intensity)
                                  (std::uint16_t, ring, ring)
                                  (std::uint16_t, clusterId, clusterId))

#endif // SRC_POINT_TYPES_H
