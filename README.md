# Perception Lidar

Le projet a été développé dans le cadre du cours machines intelligentes pour la conduction d'une voiture autonome. 

Les parties de la perception consistent en trois étapes :

1. Filtre de nuages des points
2. Clustering
3. Calcul des boîtes englobants

Ma contribution est :

1. Calcul des boîtes englobantes afin de résoudre le problème connu comme _L. Shappe_
2. Tracking en utilisant un filtre de Kaltman 
3. Estimation des classes. 

Les dossiers sont seulement une partie du projet, le projet complet appartient à l'université, ainsi le respository ne contient que les librairies développées par Oswaldo Aldair Alanis Mayorga, qui sont :

>perception_mine/src/percetion.cpp

>velodyne_utils/include/l_shappe.h

>velodyne_utils/include/kalman.h

>velodyne_utils/include/model_stimation.h